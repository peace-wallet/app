import * as bitcoin from 'bitcoinjs-lib';

import { Network } from '../models';

export const isValidAddress = (address: string, network: Network): boolean => {
  try {
    bitcoin.address.toOutputScript(
      address,
      network === 'testnet'
        ? bitcoin.networks.testnet
        : bitcoin.networks.bitcoin
    );
    return true;
  } catch (e) {
    return false;
  }
};
