import * as bitcoin from 'bitcoinjs-lib';

import { Network } from './../models/Network';
import { Utxo } from './../models/Utxo';

export const prepareTransaction = (
  amount: number,
  fee: number,
  senderAddress: string,
  receiverAddress: string,
  utxos: Utxo[],
  raws: Record<string, string>,
  node: bitcoin.ECPairInterface | bitcoin.BIP32Interface,
  network: Network
) => {
  const psbt = new bitcoin.Psbt({
    network:
      network === 'testnet'
        ? bitcoin.networks.testnet
        : bitcoin.networks.bitcoin,
  });

  let utxosAmount = 0;

  utxos.forEach((utxo) => {
    utxosAmount += utxo.amount;

    psbt.addInput({
      hash: utxo.hash,
      index: utxo.output,
      nonWitnessUtxo: Buffer.from(raws[utxo.hash], 'hex'),
    });
  });

  // Sender change
  if (utxosAmount - amount - fee > 0) {
    psbt.addOutput({
      value: utxosAmount - amount - fee,
      address: senderAddress,
    });
  }

  // Receiver output
  psbt.addOutput({
    value: amount,
    address: receiverAddress,
  });

  psbt.signAllInputs(node);
  psbt.validateSignaturesOfAllInputs();
  psbt.finalizeAllInputs();

  const hex = psbt.extractTransaction().toHex();

  return hex;
};
