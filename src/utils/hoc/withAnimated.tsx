import React from 'react';
import { Animated } from 'react-native';

export const withAnimated = <T extends object>(
  WrappedComponent: React.ComponentType<T>
) => {
  const displayName =
    WrappedComponent.displayName || WrappedComponent.name || 'Component';

  class WithAnimated extends React.Component<T> {
    static displayName = `WithAnimated(${displayName})`;

    render(): React.ReactNode {
      return <WrappedComponent {...(this.props as T)} />;
    }
  }

  return Animated.createAnimatedComponent(WithAnimated);
};
