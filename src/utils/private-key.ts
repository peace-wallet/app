import { ECPair } from './noble-ecc-wrapper';

export const isValidPrivateKey = (privateKey: string): boolean => {
  let isValid = false;

  try {
    ECPair.fromPrivateKey(Buffer.from(privateKey, 'hex'), {
      compressed: false,
    });
    isValid = true;
  } catch (e) {
    isValid = false;
  }

  return isValid;
};

export const isValidWIF = (wif: string): boolean => {
  let isValid = false;

  try {
    ECPair.fromWIF(wif);
    isValid = true;
  } catch (e) {
    isValid = false;
  }

  return isValid;
};
