import * as bip39 from 'bip39';

export const generateMnemonic = () => bip39.generateMnemonic();

export const isValidMnemonic = (mnemonic: string): boolean =>
  bip39.validateMnemonic(mnemonic);
