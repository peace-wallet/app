import { Balance, Coin } from '../models';
import { CoinsFormatter } from './formatters';

export const balancesByCoin = (
  coin: Coin,
  balances: Balance[],
  options: { shardId?: number; excludeEmpty?: boolean } = {}
): Balance[] | undefined => {
  const { shardId = undefined, excludeEmpty = false } = options;

  const maxInt32Value = Math.pow(2, 32) - 1;

  if (shardId !== undefined && shardId !== null) {
    return balances.filter((balance) => balance.shardId === shardId);
  }

  if (coin.name === 'btc') {
    return balances.filter((balance) => balance.shardId === coin.shardId);
  }

  if (coin.name === 'jax') {
    if (excludeEmpty) {
      return balances.filter(
        (balanceByShard) =>
          balanceByShard.shardId !== 0 &&
          balanceByShard.shardId !== maxInt32Value &&
          balanceByShard.balance > 0
      );
    }

    return balances.filter(
      (balance) => balance.shardId !== 0 && balance.shardId !== maxInt32Value
    );
  }

  if (coin.name === 'jxn') {
    return balances.filter((balance) => balance.shardId === coin.shardId);
  }
};

export const balanceByShardId = (
  shardId: number,
  balances: Balance[]
): Balance => {
  if (shardId === null || shardId === undefined || !balances.length) return;

  return balances.find((balance) => balance.shardId === shardId);
};

export const balanceByCoin = (
  coin: Coin,
  balances: Balance[]
): {
  available: number;
  locked: number;
  total: number;
} => {
  const _balances = balancesByCoin(coin, balances);

  const availableBalance = _balances.reduce((a, b) => a + b.balance, 0) ?? 0;
  const lockedBalance = _balances.reduce((a, b) => a + b.balanceLocked, 0) ?? 0;
  const totalBalance = availableBalance + lockedBalance ?? 0;

  return {
    available: CoinsFormatter.toBiggestUnits(coin.name, availableBalance),
    locked: CoinsFormatter.toBiggestUnits(coin.name, lockedBalance),
    total: CoinsFormatter.toBiggestUnits(coin.name, totalBalance),
  };
};
