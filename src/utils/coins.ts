import { Coin } from '../models';

export const getCoins = (): Coin[] => [
  // {
  //   name: 'btc',
  //   title: 'BTC',
  //   shardId: Math.pow(2, 32) - 1,
  // },
  {
    name: 'jax',
    title: 'JAX',
    shardId: null,
  },
  {
    name: 'jxn',
    title: 'JXN',
    shardId: 0,
  },
];

export const getCoinByShardId = (shardId: number): Coin => {
  // const maxInt32Value = Math.pow(2, 32) - 1;

  // if (shardId === maxInt32Value) {
  //   return getCoins().find((c) => c.name === 'btc');
  // } else
  
  if (shardId === 0) {
    return getCoins().find((c) => c.name === 'jxn');
  } else {
    return getCoins().find((c) => c.name === 'jax');
  }
};
