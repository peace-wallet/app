import { storage } from '../utils/storage';
import * as bip39 from 'bip39';
import * as bitcoin from 'bitcoinjs-lib';
import Config from 'react-native-config';
import { create } from 'zustand';

import { Network } from '../models';
import { api } from '../services/api';
import { ECPair, bip32, ECPairInterface, BIP32Interface } from '../utils/noble-ecc-wrapper';

import { isValidPrivateKey, isValidWIF } from './../utils/private-key';

type AppState = {
  address: string | null;
  mnemonic: string | null;
  privateKey: string | null;
  node: ECPairInterface | BIP32Interface | null;
  network: Network;

  pinCode: string | null;
  pinCodeScreenIsVisible: boolean;

  loadMnemonic: () => Promise<string | null>;
  saveMnemonic: (mnemonic: string) => Promise<void>;

  loadPrivateKey: () => Promise<string | null>;
  savePrivateKey: (privateKey: string) => Promise<void>;

  loadPinCode: () => Promise<string | null>;
  savePinCode: (pinCode: string) => Promise<void>;
  removePinCode: () => Promise<void>;

  showPinCodeScreen: () => void;
  hidePinCodeScreen: () => void;

  setNetwork: (network: Network) => void;
  toggleNetwork: () => void;

  logout: () => Promise<void>;

  _fillAddress: () => Promise<void>;
};

const INITIAL_NETWORK: Network = 'testnet';

const useAppStore = create<AppState>((set, get) => ({
  address: null,
  mnemonic: null,
  privateKey: null,
  node: null,
  network: INITIAL_NETWORK,

  pinCode: null,
  pinCodeScreenIsVisible: false,

  saveMnemonic: async mnemonic => {
    await storage.saveString('mnemonic', mnemonic);

    set(state => ({ ...state, mnemonic }));
    await get()._fillAddress();
  },

  loadMnemonic: async () => {
    const mnemonic = await storage.loadString('mnemonic');

    console.log('mnemonic', mnemonic)

    set(state => ({ ...state, mnemonic }));
    await get()._fillAddress();

    return mnemonic;
  },

  savePrivateKey: async privateKey => {
    await storage.saveString('privateKey', privateKey);

    set(state => ({ ...state, privateKey }));
    await get()._fillAddress();
  },

  loadPrivateKey: async () => {
    const privateKey = await storage.loadString('privateKey');

    set(state => ({ ...state, privateKey }));
    await get()._fillAddress();

    return privateKey;
  },

  savePinCode: async pinCode => {
    await storage.saveString('pinCode', pinCode);

    set(state => ({ ...state, pinCode }));
  },

  loadPinCode: async () => {
    const pinCode = await storage.loadString('pinCode');

    set(state => ({ ...state, pinCode }));

    return pinCode;
  },

  removePinCode: async () => {
    await storage.remove('pinCode');

    set(state => ({ ...state, pinCode: null }));
  },

  showPinCodeScreen: () => {
    set(state => ({ ...state, pinCodeScreenIsVisible: true }));
  },

  hidePinCodeScreen: () => {
    set(state => ({ ...state, pinCodeScreenIsVisible: false }));
  },

  setNetwork: network => {
    set(state => ({
      ...state,
      network,
    }));

    api.defaults.baseURL =
      network === 'mainnet' ? Config.MAINNET_API_URL : Config.TESTNET_API_URL;

    console.log('setNetwork: network:', network);
    console.log('Config.MAINNET_API_URL:', Config.MAINNET_API_URL);
    console.log('Config.TESTNET_API_URL:', Config.TESTNET_API_URL);
  },

  toggleNetwork: async () => {
    const { address, network, setNetwork, _fillAddress } = get();

    setNetwork(network === 'testnet' ? 'mainnet' : 'testnet');

    await _fillAddress();

    console.log(
      'toggleNetwork: network:',
      network === 'testnet' ? 'mainnet' : 'testnet',
    );
    console.log('toggleNetwork: address:', address);
    console.log('toggleNetwork: baseURL:', api.defaults.baseURL);
  },

  logout: async () => {
    set({
      address: null,
      mnemonic: null,
      privateKey: null,
      pinCode: null,
      network: INITIAL_NETWORK,
    });

    await storage.clear();
  },

  _fillAddress: async () => {
    const { mnemonic, privateKey, network } = get();

    if (!mnemonic && !privateKey) return;

    const MAINNET_PATH = "m/44'/0'/0'/0/0";
    const TESTNET_PATH = "m/44'/1'/0'/0/0";

    const path = network === 'testnet' ? TESTNET_PATH : MAINNET_PATH;

    let seed = null;
    let node: ECPairInterface | BIP32Interface | null = null;

    if (mnemonic) {
      seed = await bip39.mnemonicToSeed(mnemonic);
      node = bip32.fromSeed(seed).derivePath(path);
    }

    if (privateKey && isValidPrivateKey(privateKey)) {
      node = ECPair.fromPrivateKey(Buffer.from(privateKey, 'hex'), {
        compressed: false,
      });
    }

    if (privateKey && isValidWIF(privateKey)) {
      node = ECPair.fromWIF(privateKey);
    }

    const address = bitcoin.payments.p2pkh({
      pubkey: node?.publicKey,
      network:
        network === 'testnet'
          ? bitcoin.networks.testnet
          : bitcoin.networks.bitcoin,
    }).address;

    set(state => ({
      ...state,
      address,
      node,
    }));
  },
}));

export { useAppStore };
