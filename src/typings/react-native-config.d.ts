declare module 'react-native-config' {
    export interface NativeConfig {
        MAINNET_API_URL?: string;
        TESTNET_API_URL?: string;
    }
    
    export const Config: NativeConfig
    export default Config
  }