import { Toast } from '../components/base';
import { useQuery } from '@tanstack/react-query';
import { AxiosError } from 'axios';
import { t } from 'i18next';

import { Balance, Network } from '../models';
import { BaseError, getBalances } from '../services/api';

const useBalances = (address: string, network: Network) => {
  return useQuery<Balance[], AxiosError<BaseError>>(
    ['balances', network, address],
    ({ signal }) => {
      console.log('useUserBalances start');
      return getBalances({ address, signal });
    },
    {
      initialData: [],
      enabled: address !== null,
      refetchInterval: 120000,
      retry: false,
      select: balances => balances.sort((a, b) => a.shardId - b.shardId),
      onError: error => {
        Toast.show({
          type: 'error',
          text1: t('notifications.error'),
          text2: `getBalance: ${error.message}`,
        });
      },
    },
  );
};

export { useBalances };
