import { Toast } from '../components/base';
import { useQuery } from '@tanstack/react-query';
import { AxiosError } from 'axios';
import { t } from 'i18next';

import { Network } from '../models';
import {
  BaseError,
  UtxosAndFeesResponse,
  getUtxosAndFees,
} from '../services/api';

const useUtxosAndFees = (
  shardId: number,
  amount: number,
  address: string,
  network: Network,
  enabled?: boolean,
) => {
  return useQuery<UtxosAndFeesResponse, AxiosError<BaseError>>(
    ['utxos-and-fees', network, address, amount, shardId],
    () => getUtxosAndFees(shardId, amount, address),
    {
      enabled:
        shardId !== null &&
        shardId !== undefined &&
        amount !== null &&
        amount !== undefined &&
        amount > 0 &&
        address !== null &&
        address !== undefined &&
        enabled,
      keepPreviousData: true,
      onError: error => {
        Toast.show({
          type: 'error',
          text1: t('notifications.error'),
          text2: `getUtxosAndFees: ${error.message}`,
        });
      },
    },
  );
};

export { useUtxosAndFees };
