import { Toast } from '../components/base';
import { InfiniteData, useInfiniteQuery } from '@tanstack/react-query';
import { AxiosError } from 'axios';
import { t } from 'i18next';
import moment from 'moment';

import { HistoryByShard, Network } from '../models';
import { BaseError, getHistory } from '../services/api';
import { HistoryItem } from './../models/HistoryItem';

const LIMIT = 30;

function sliceIntoChunks(arr, chunkSize) {
  const res = [];
  for (let i = 0; i < arr.length; i += chunkSize) {
    const chunk = arr.slice(i, i + chunkSize);
    res.push(chunk);
  }
  return res;
}

const useHistory = (
  address: string,
  shardIds: number[],
  network: Network,
  limit = LIMIT,
) => {
  return useInfiniteQuery<
    HistoryByShard[],
    AxiosError<BaseError>,
    HistoryItem[][]
  >(
    ['history', network, address, shardIds],
    async ({ signal, pageParam = 0 }) => {
      const response = await Promise.all(
        shardIds.map(shardId => {
          const _limit = [1, 2, 3].includes(shardId) ? 10 : limit;

          return getHistory({
            address,
            shardId,
            offset: pageParam * _limit,
            limit: _limit,
            signal,
          });
        }),
      );

      const history = response.flat();

      return history;
    },
    {
      initialData: { pages: [], pageParams: [] },
      enabled: address !== null && shardIds.length > 0,
      refetchInterval: 120000,
      retry: false,
      cacheTime: 0,
      select: (data: InfiniteData<HistoryByShard[]>) => {
        const sections: HistoryItem[][] = [];
        const pageParams = [];

        data.pages.forEach((page, pageIndex) => {
          pageParams.push([]);

          page.forEach(historyByShard => {
            pageParams[pageIndex].push({
              shardId: historyByShard.shardId,
              loadedAll: historyByShard.loadedAll,
            });

            historyByShard.items.forEach(item => {
              const date = moment(item.timestamp).format('YYYY-MM-DD');
              const direction = item.amount > 0 ? 'in' : 'out';

              item = {
                ...item,
                hash: item.id ?? item.hash,
                amount: Math.abs(item.amount),
                shardId: historyByShard.shardId,
                direction,
              };

              const section = sections.find(
                _section =>
                  moment(_section[0].timestamp).format('YYYY-MM-DD') === date,
              );

              if (section) {
                section.push(item);
                section.sort((a, b) => (a.timestamp < b.timestamp ? 1 : -1));
              } else {
                sections.push([item]);
              }
            });
          });
        });

        return {
          pageParams: pageParams,
          pages: sliceIntoChunks(sections, data.pages.length),
        };
      },
      getPreviousPageParam: () => undefined,
      getNextPageParam: (lastPage, pages) => {
        const lastData = lastPage?.flat() ?? [];
        const data = pages.flat();

        if (lastData.length >= limit) {
          return data.length / limit;
        } else {
          return undefined;
        }
      },
      onError: error => {
        Toast.show({
          type: 'error',
          text1: t('notifications.error'),
          text2: `getHistory: ${error.message}`,
        });
      },
    },
  );
};

export { useHistory };
