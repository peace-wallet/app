import { Toast } from '../components/base';
import { useMutation } from '@tanstack/react-query';
import { AxiosError } from 'axios';
import { t } from 'i18next';

import { BaseError, getTransactionsRaw } from '../services/api';

const useTransactionsRaws = () => {
  return useMutation<
    Record<string, string>,
    AxiosError<BaseError>,
    {
      shardId: number;
      hashes: string[];
    }
  >(
    ['transactions-raws'],
    async ({ shardId, hashes }) => {
      const chunkSize = 32;

      const chunks = [];

      for (let i = 0; i < hashes.length; i += chunkSize) {
        const chunk = hashes.slice(i, i + chunkSize);
        chunks.push(chunk);
      }

      const response = await Promise.all(
        chunks.map(chunk => getTransactionsRaw(shardId, chunk)),
      );

      const raws = Object.assign({}, ...response);

      return raws;
    },
    {
      onError: error => {
        Toast.show({
          type: 'error',
          text1: t('notifications.error'),
          text2: `getTransactionsRaw: ${error.message}`,
        });
      },
    },
  );
};

export { useTransactionsRaws };
