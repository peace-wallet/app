import { Toast } from '../components/base';
import { useQuery } from '@tanstack/react-query';
import { AxiosError } from 'axios';
import { t } from 'i18next';

import { Balance, Network } from '../models';
import { BaseError, getBtcBalance } from '../services/api';

const useBtcBalance = (address: string, network: Network) => {
  return useQuery<number, AxiosError<BaseError>, Balance>(
    ['btc-balance', network, address],
    ({ signal }) => getBtcBalance(address, signal),
    {
      initialData: null,
      enabled: address !== null || address !== undefined,
      refetchInterval: 120000,
      retry: false,
      select: data => ({
        shardId: Math.pow(2, 32) - 1,
        balance: data ?? 0,
        balanceLocked: 0,
      }),
      onError: error => {
        Toast.show({
          type: 'error',
          text1: t('notifications.error'),
          text2: `getBtcBalance: ${error.message}`,
        });
      },
    },
  );
};

export { useBtcBalance };
