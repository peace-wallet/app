import { Toast } from '../components/base';
import { useMutation } from '@tanstack/react-query';
import { AxiosError } from 'axios';
import { t } from 'i18next';

import { BaseError, publishTransaction } from '../services/api';

const useTransactionPublish = () => {
  return useMutation<
    string,
    AxiosError<BaseError>,
    { shardId: number; hex: string }
  >(
    ['transaction-publish'],
    ({ hex, shardId }) => {
      return publishTransaction(shardId, hex);
    },
    {
      onError: error => {
        Toast.show({
          type: 'error',
          text1: t('notifications.error'),
          text2: `publishTransaction: ${error.message}`,
        });
      },
    },
  );
};

export { useTransactionPublish };
