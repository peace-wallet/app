import { Toast } from '../components/base';
import { useQuery } from '@tanstack/react-query';
import { AxiosError } from 'axios';
import { t } from 'i18next';

import { Balance, Network } from '../models';
import { BaseError, getJaxBalances } from '../services/api';
import { BalanceBackend } from './../models/BalanceBackend';

const useJaxBalances = (address: string, network: Network) => {
  return useQuery<BalanceBackend[], AxiosError<BaseError>, Balance[]>(
    ['jax-balances', network, address],
    ({ signal }) => {
      return getJaxBalances({ address, signal });
    },
    {
      initialData: [],
      enabled: address !== null && address !== undefined,
      refetchInterval: 120000,
      retry: false,
      select: data => {
        const balances = data
          .map(balance => ({
            shardId: balance.shardID,
            balance: balance?.balance,
            balanceLocked: balance?.balanceLocked,
          }))
          .sort((a, b) => a.shardId - b.shardId);

        return balances;
      },
      onError: error => {
        Toast.show({
          type: 'error',
          text1: t('notifications.error'),
          text2: `getJaxBalances: ${error.message}`,
        });
      },
    },
  );
};

export { useJaxBalances };
