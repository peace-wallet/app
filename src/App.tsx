import { theme } from './theme';
import './i18n';
import { ToastsContainer } from './components/base';
import NetInfo from '@react-native-community/netinfo';
import {
  QueryClientProvider,
  focusManager,
  onlineManager,
} from '@tanstack/react-query';
import { NativeBaseProvider } from 'native-base';
import React, { useCallback, useEffect, useRef, useState } from 'react';
import { useTranslation } from 'react-i18next';
import { Alert, AppState, Platform } from 'react-native';
import type { AppStateStatus } from 'react-native';
import RNBootSplash from 'react-native-bootsplash';
import { GestureHandlerRootView } from 'react-native-gesture-handler';
import { SafeAreaProvider } from 'react-native-safe-area-context';

import { PinCodeLock, PrivacyBlur } from './components/features';
import { RootNavigator } from './navigators';
import { queryClient } from './services/react-query';
import { useAppStore } from './stores';

export const App = () => {
  const { t } = useTranslation();

  const mnemonic = useAppStore(state => state.mnemonic);
  const privateKey = useAppStore(state => state.privateKey);

  const pinCodeScreenIsVisible = useAppStore(
    state => state.pinCodeScreenIsVisible,
  );

  const setNetwork = useAppStore(state => state.setNetwork);
  const loadMnemonic = useAppStore(state => state.loadMnemonic);
  const loadPrivateKey = useAppStore(state => state.loadPrivateKey);
  const loadPinCode = useAppStore(state => state.loadPinCode);
  const showPinCodeScreen = useAppStore(state => state.showPinCodeScreen);

  const offlineModalIsVisible = useRef<boolean>(false);

  const [showOnboardingScreen, setShowOnboardingScreen] = useState(true);

  const showNetworkModal = useCallback(() => {
    offlineModalIsVisible.current = true;

    Alert.alert(t('titles.offline'), t('texts.offline'), [
      {
        text: t('buttons.tryAgain'),
        onPress: () => {
          offlineModalIsVisible.current = false;
          NetInfo.refresh().then(state => {
            if (!state.isConnected && !offlineModalIsVisible.current) {
              showNetworkModal();
            }
          });
        },
      },
    ]);
  }, [t]);

  const onAppStateChange = (status: AppStateStatus) => {
    if (Platform.OS !== 'web') {
      focusManager.setFocused(status === 'active');
    }
  };

  useEffect(() => {
    (async () => {
      setNetwork('testnet');

      await Promise.all([loadMnemonic(), loadPrivateKey()]);

      const pinCode = await loadPinCode();

      if (pinCode) {
        showPinCodeScreen();
      }

      setTimeout(() => {
        RNBootSplash.hide({ fade: true });
      }, 500);
    })();
  }, [
    setNetwork,
    loadMnemonic,
    loadPrivateKey,
    loadPinCode,
    showPinCodeScreen,
  ]);

  useEffect(() => {
    setShowOnboardingScreen(!mnemonic && !privateKey);
  }, [mnemonic, privateKey]);

  useEffect(() => {
    onlineManager.setEventListener(setOnline => {
      return NetInfo.addEventListener(state => {
        setOnline(!!state.isConnected);
      });
    });

    const subscription = AppState.addEventListener('change', onAppStateChange);

    return () => {
      subscription.remove();
    };
  }, []);

  useEffect(() => {
    const unsubscribe = NetInfo.addEventListener(state => {
      if (!state.isConnected && !offlineModalIsVisible.current) {
        showNetworkModal();
      }
    });

    return () => {
      unsubscribe();
    };
  }, [showNetworkModal]);

  return (
    <NativeBaseProvider theme={theme}>
      <QueryClientProvider client={queryClient}>
        <SafeAreaProvider>
          <GestureHandlerRootView style={{ flex: 1 }}>
            <RootNavigator showOnboardingScreen={showOnboardingScreen} />

            <ToastsContainer />
            {/* <PrivacyBlur /> */}
            {/* <PinCodeLock visible={pinCodeScreenIsVisible} /> */}
          </GestureHandlerRootView>
        </SafeAreaProvider>
      </QueryClientProvider>
    </NativeBaseProvider>
  );
};
