export type Balance = {
  shardId: number;
  balance: number;
  balanceLocked: number;
};
