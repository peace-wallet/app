export type Utxo = {
  amount: number;
  hash: string;
  output: number;
  raw?: string;
};
