export type Coin = {
  name: 'jax' | 'jxn' | 'btc';
  title: string;
  shardId: number;
};
