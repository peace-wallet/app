export type HistoryItem = {
  id?: string;
  confirmations: number;
  hash: string;
  crossShardTx?: boolean;
  amount: number;
  sentAmount: number;
  receivedAmount: number;
  direction: 'in' | 'out';
  timestamp: string;
  shardId: number;
};
