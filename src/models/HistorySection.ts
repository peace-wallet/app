import { HistoryItem } from './HistoryItem';

export type HistorySection = {
  data: HistoryItem[][];
  loadedAll: boolean;
};
