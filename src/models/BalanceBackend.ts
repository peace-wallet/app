import { Balance } from './Balance';

export type BalanceBackend = Omit<Balance, 'shardId'> & { shardID: number };
