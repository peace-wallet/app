import { HistoryItem } from './HistoryItem';

export type HistoryByShard = {
  shardId: number;
  loadedAll: boolean;
  items: HistoryItem[];
};
