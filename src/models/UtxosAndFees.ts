import { Fee } from './Fee';
import { Utxo } from './Utxo';

export type UtxosAndFees = {
  utxos: Utxo[];
  fees: Fee[];
};
