export type Fee = {
  name: 'fast' | 'moderate' | 'slow';
  value: number;
  maxAmount: number;
};
