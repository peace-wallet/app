import {
  Button,
  Card,
  HStack,
  Header,
  Icon,
  Screen,
  ScreenContent,
  Spacer,
  Text,
  TextArea,
  VStack,
} from '../components/base';
import Clipboard from '@react-native-clipboard/clipboard';
import React, { useState } from 'react';
import { Controller, useForm } from 'react-hook-form';
import { useTranslation } from 'react-i18next';

import { useAppStore } from '../stores';
import { isValidPrivateKey, isValidWIF } from '../utils/private-key';

interface FormData {
  privateKey: string;
}

export const WalletRestoreByPrivateKeyScreen = () => {
  const { t } = useTranslation();

  const [isLoading, setIsLoading] = useState(false);

  const savePrivateKey = useAppStore(state => state.savePrivateKey);
  const showPinCodeScreen = useAppStore(state => state.showPinCodeScreen);

  const {
    control,
    setValue,
    trigger,
    handleSubmit,
    formState: { errors },
  } = useForm<FormData>({
    mode: 'all',
  });

  const onSubmit = async (data: FormData) => {
    const { privateKey } = data;

    setIsLoading(true);
    await savePrivateKey(privateKey);
    setIsLoading(false);
    showPinCodeScreen();
  };

  const onPasteFromClipboard = async () => {
    const text = (await Clipboard.getString()).replace(/\n/g, ' ').trim();
    setValue('privateKey', text);
    trigger('privateKey');
  };

  const renderPrivateKeyCaption = () => {
    let text: string = null;

    switch (errors.privateKey?.type) {
      case 'required':
        text = t('errors.privateKeyIsRequired');
        break;
      case 'valid':
        text = t('errors.privateKeyIsInvalid');
        break;
      default:
        text = null;
        break;
    }

    return (
      text && (
        <HStack px="4" alignItems="center" space={2}>
          <Icon.Info size={3.5} color="red.500" />
          <Text fontSize="sm" color="red.500">
            {text}
          </Text>
        </HStack>
      )
    );
  };

  return (
    <Screen header={<Header />}>
      <ScreenContent>
        <VStack flex={1} space="sm">
          <Text fontSize={28} align="center">
            {t('titles.walletRestore')}
          </Text>

          <VStack space="xs">
            <Card p="2" error={!!errors.privateKey}>
              <Controller
                control={control}
                render={({ field: { onChange, onBlur, value } }) => (
                  <TextArea
                    value={value}
                    variant="unstyled"
                    totalLines={3}
                    p="2"
                    h="32"
                    size="lg"
                    error={!!errors.privateKey}
                    placeholder={t('placeholders.walletRestoreByPrivateKey')}
                    onBlur={onBlur}
                    onChangeText={onChange}
                  />
                )}
                name="privateKey"
                rules={{
                  required: true,
                  validate: {
                    valid: value =>
                      isValidPrivateKey(value) || isValidWIF(value),
                  },
                }}
              />
            </Card>

            {renderPrivateKeyCaption()}

            <HStack justifyContent="center">
              <Button
                size="md"
                variant="subtle"
                startIcon={<Icon.Paste size={5} />}
                onPress={onPasteFromClipboard}>
                {t('buttons.paste')}
              </Button>
            </HStack>
          </VStack>

          <Spacer />

          <Button isLoading={isLoading} onPress={handleSubmit(onSubmit)}>
            {t('buttons.restoreWallet')}
          </Button>
        </VStack>
      </ScreenContent>
    </Screen>
  );
};
