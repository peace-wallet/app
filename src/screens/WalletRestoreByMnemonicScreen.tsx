import {
  Button,
  Card,
  HStack,
  Header,
  Icon,
  Screen,
  ScreenContent,
  Spacer,
  Text,
  TextArea,
  VStack,
} from '../components/base';
import Clipboard from '@react-native-clipboard/clipboard';
import { useNavigation } from '@react-navigation/native';
import React, { useState } from 'react';
import { Controller, useForm } from 'react-hook-form';
import { useTranslation } from 'react-i18next';

import { useAppStore } from '../stores';
import { isValidMnemonic } from '../utils/mnemonic';

export const WalletRestoreByMnemonicScreen = () => {
  const { t } = useTranslation();
  const navigation = useNavigation();

  const [isLoading, setIsLoading] = useState(false);

  const saveMnemonic = useAppStore(state => state.saveMnemonic);
  const showPinCodeScreen = useAppStore(state => state.showPinCodeScreen);

  const {
    control,
    setValue,
    trigger,
    handleSubmit,
    reset,
    formState: { errors },
  } = useForm({
    mode: 'onSubmit',
  });

  const navigateToRestoreByPrivateKeyScreen = () =>
    navigation.navigate('OnboardingStack', {
      screen: 'WalletStack',
      params: {
        screen: 'WalletRestoreByPrivateKey',
      },
    });

  const onSubmit = async data => {
    const { mnemonic } = data;

    setIsLoading(true);
    await saveMnemonic(mnemonic);
    setIsLoading(false);
    showPinCodeScreen();
  };

  const onPasteFromClipboard = async () => {
    const text = (await Clipboard.getString()).replace(/\n/g, ' ').trim();
    setValue('mnemonic', text);
    trigger('mnemonic');
  };

  const renderMnemonicCaption = () => {
    let text: string = null;

    switch (errors.mnemonic?.type) {
      case 'required':
        text = t('errors.mnemonicIsRequired');
        break;
      case 'valid':
        text = t('errors.mnemonicIsInvalid');
        break;
      default:
        text = null;
        break;
    }

    return (
      text && (
        <HStack px="4" alignItems="center" space={2}>
          <Icon.Info size={3.5} color="red.500" />
          <Text fontSize="sm" color="red.500">
            {text}
          </Text>
        </HStack>
      )
    );
  };

  return (
    <Screen header={<Header />}>
      <ScreenContent>
        <VStack flex={1} space="sm">
          <Text fontSize={28} align="center">
            {t('titles.walletRestore')}
          </Text>

          <VStack space="xs">
            <Card p="2" error={!!errors.mnemonic}>
              <Controller
                control={control}
                render={({ field: { onChange, onBlur, value } }) => (
                  <TextArea
                    value={value}
                    variant="unstyled"
                    p="2"
                    h="32"
                    size="lg"
                    error={!!errors.mnemonic}
                    placeholder={t('placeholders.walletRestoreByMnemonic')}
                    onBlur={onBlur}
                    onChangeText={text => {
                      reset({ mnemonic: text });
                      onChange(text);
                    }}
                  />
                )}
                name="mnemonic"
                rules={{
                  required: true,
                  validate: {
                    valid: isValidMnemonic,
                  },
                }}
              />
            </Card>

            {renderMnemonicCaption()}

            <HStack justifyContent="center">
              <Button
                size="md"
                variant="subtle"
                startIcon={<Icon.Paste size={5} />}
                onPress={onPasteFromClipboard}>
                {t('buttons.paste')}
              </Button>
            </HStack>
          </VStack>

          <Spacer />

          <VStack space="sm">
            <Button
              variant="ghost"
              onPress={navigateToRestoreByPrivateKeyScreen}>
              {t('buttons.restoreWalletByPrivateKey')}
            </Button>
            <Button isLoading={isLoading} onPress={handleSubmit(onSubmit)}>
              {t('buttons.restoreWallet')}
            </Button>
          </VStack>
        </VStack>
      </ScreenContent>
    </Screen>
  );
};
