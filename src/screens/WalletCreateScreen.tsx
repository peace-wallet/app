import {
  Button,
  HStack,
  Header,
  Icon,
  Screen,
  ScreenContent,
  Spacer,
  Text,
  Toast,
  VStack,
} from '../components/base';
import Clipboard from '@react-native-clipboard/clipboard';
import React, { useState } from 'react';
import { useTranslation } from 'react-i18next';

import { MnemonicPhrase } from '../components/features';
import { useAppStore } from '../stores';
import { generateMnemonic } from '../utils/mnemonic';

export const WalletCreateScreen = () => {
  const { t } = useTranslation();

  const saveMnemonic = useAppStore(state => state.saveMnemonic);
  const showPinCodeScreen = useAppStore(state => state.showPinCodeScreen);

  const [isLoading, setIsLoading] = useState(false);
  const [mnemonic, setMnemonic] = useState(generateMnemonic());

  const onSubmit = async () => {
    setIsLoading(true);
    await saveMnemonic(mnemonic);
    setIsLoading(false);
    showPinCodeScreen();
  };

  const onCopyPhrase = () => {
    Clipboard.setString(mnemonic);

    Toast.show({
      type: 'info',
      text1: t('notifications.copied'),
    });
  };

  const onNewPhrase = () => setMnemonic(generateMnemonic());

  return (
    <Screen header={<Header />}>
      <ScreenContent>
        <VStack flex={1} space="lg">
          <Text fontSize={28} align="center">
            {t('titles.walletCreate')}
          </Text>

          <HStack justifyContent="center" px="2">
            <Text align="center" secondary>
              {t('texts.walletCreate')}
            </Text>
          </HStack>

          <MnemonicPhrase mnemonic={mnemonic} />

          <Spacer />

          <VStack space="md">
            <HStack space="sm">
              <Button
                borderRadius="md"
                flex={1}
                variant="outline"
                endIcon={<Icon.Copy />}
                onPress={onCopyPhrase}>
                {t('buttons.copyPhrase')}
              </Button>
              <Button
                borderRadius="md"
                flex={1}
                variant="outline"
                endIcon={<Icon.Refresh />}
                onPress={onNewPhrase}>
                {t('buttons.newPhrase')}
              </Button>
            </HStack>
            <Button isLoading={isLoading} onPress={onSubmit}>
              {t('buttons.savePhrase')}
            </Button>
          </VStack>
        </VStack>
      </ScreenContent>
    </Screen>
  );
};
