import { Button, HStack, Header, Modal, Text } from '../components/base';
import moment from 'moment';
import React, { useState } from 'react';
import { useTranslation } from 'react-i18next';

import {
  HistoryList,
  Screen,
  TransactionDetails,
} from '../components/features';
import { HistoryItem } from '../models';
import { useAppStore } from '../stores';
import { getCoins } from '../utils/coins';

export const BalanceScreen = () => {
  const { t } = useTranslation();

  const [coin, setCoin] = useState(getCoins()[1]);

  const network = useAppStore(state => state.network);

  const [selectedHistoryItem, setSelectedHistoryItem] =
    useState<HistoryItem | null>(null);

  const renderHistoryDetailsModal = () => (
    <Modal
      isVisible={!!selectedHistoryItem}
      HeaderComponent={
        selectedHistoryItem && (
          <HStack justifyContent="space-between">
            <Text textTransform="uppercase">
              {selectedHistoryItem.direction === 'in'
                ? t('labels.incoming')
                : t('labels.outgoing')}
            </Text>
            <Text
              _light={{ color: 'primaryAlpha.500' }}
              _dark={{ color: 'whiteAlpha.500' }}>
              {moment(selectedHistoryItem.timestamp).format('DD MMM, LT')}
            </Text>
          </HStack>
        )
      }
      FooterComponent={
        <HStack space="xs">
          <Button
            flex={1}
            variant="outline"
            onPress={() => setSelectedHistoryItem(null)}>
            {t('buttons.close')}
          </Button>
        </HStack>
      }
      onClosed={() => setSelectedHistoryItem(null)}>
      {selectedHistoryItem && (
        <TransactionDetails
          coin={coin}
          network={network}
          sourceAmount={selectedHistoryItem.amount}
          // destinationAmount={selectedHistoryItem.destinationAmount}
          // transactionFee={selectedHistoryItem.transactionFee}
          // exchangeAgentFee={selectedHistoryItem.exchangeAgentFee}
          address={''}
          sourceShardId={selectedHistoryItem.shardId}
          // destinationShardID={selectedHistoryItem.destinationShardID}
          hash={selectedHistoryItem.hash}
          confirmations={selectedHistoryItem.confirmations}
          direction={selectedHistoryItem.direction}
          // blockNumber={selectedHistoryItem.blockNumber}
          // lockTransactionHash={selectedHistoryItem.lockTransactionHash}
        />
      )}
    </Modal>
  );

  return (
    <Screen
      testID="balance-screen"
      unsafeBottom
      header={
        <Header
          hasBackAction={false}
          // rightAction={
          //   <IconButton
          //     variant="unstyled"
          //     icon={<Icon.ScanQR />}
          //     onPress={navigateToSendScanQRScreen}
          //   />
          // }
        >
          {t('titles.balance')}
        </Header>
      }>
      {renderHistoryDetailsModal()}

      <HistoryList
        onSelectCoin={setCoin}
        onSelectHistoryItem={setSelectedHistoryItem}
      />
    </Screen>
  );
};
