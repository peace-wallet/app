import {
  Button,
  HStack,
  Header,
  Icon,
  Screen,
  ScreenContent,
  Spacer,
  Text,
  Toast,
  VStack,
} from '../components/base';
import Clipboard from '@react-native-clipboard/clipboard';
import React from 'react';
import { useTranslation } from 'react-i18next';

import { MnemonicPhrase } from '../components/features';
import { useAppStore } from '../stores';

export const WalletBackupScreen = () => {
  const { t } = useTranslation();
  const mnemonic = useAppStore(state => state.mnemonic);

  const onCopyPhrase = () => {
    Clipboard.setString(mnemonic);
    Toast.show({
      type: 'info',
      text1: 'Copied',
    });
  };

  return (
    <Screen header={<Header />}>
      <ScreenContent>
        <VStack flex={1} space="lg">
          <Text fontSize={28} align="center">
            {t('titles.walletBackup')}
          </Text>

          <HStack justifyContent="center" px="2">
            <Text align="center" secondary>
              {t('texts.walletBackup')}
            </Text>
          </HStack>

          <MnemonicPhrase mnemonic={mnemonic} />

          <Spacer />

          <Button
            variant="outline"
            endIcon={<Icon.Copy />}
            onPress={onCopyPhrase}>
            {t('buttons.copyPhrase')}
          </Button>
        </VStack>
      </ScreenContent>
    </Screen>
  );
};
