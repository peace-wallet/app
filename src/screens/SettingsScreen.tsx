import {
  Button,
  Card,
  Divider,
  HStack,
  Header,
  Icon,
  Modal,
  Pressable,
  ScreenContent,
  Switch,
  Text,
  VStack,
} from '../components/base';
import { StackActions, useNavigation } from '@react-navigation/native';
import { View, useColorModeValue } from 'native-base';
import React, { useState } from 'react';
import { Trans, useTranslation } from 'react-i18next';
import DeviceInfo from 'react-native-device-info';

import { Screen } from '../components/features';
import { useAppStore } from '../stores';

export const SettingsScreen = () => {
  const navigation = useNavigation();
  const { t } = useTranslation();

  const iconColorToken = useColorModeValue(
    'primaryAlpha.200',
    'whiteAlpha.200',
  );
  const labelColorToken = useColorModeValue(
    'primaryAlpha.500',
    'whiteAlpha.500',
  );

  const pinCode = useAppStore(state => state.pinCode);
  const network = useAppStore(state => state.network);
  const mnemonic = useAppStore(state => state.mnemonic);

  const logout = useAppStore(state => state.logout);
  const toggleNetwork = useAppStore(state => state.toggleNetwork);
  const removePinCode = useAppStore(state => state.removePinCode);
  const showPinCodeScreen = useAppStore(state => state.showPinCodeScreen);

  const [exiting, setIsExiting] = useState(false);
  const [networkTypeIsChanging, setNetworkTypeIsChanging] = useState(false);
  const [
    logoutConfirmationModalIsVisible,
    setLogoutConfirmationModalIsVisible,
  ] = useState(false);
  const [changeNetworkModalIsVisible, setChangeNetworkModalIsVisible] =
    useState(false);

  const hasUnsavedRefunds = () => {
    const unsavedRefundsCount = 0;

    // localHistory.forEach((history) => {
    //   history.items.forEach((historyItem) => {
    //     if (historyItem.isCrossShard) {
    //       unsavedRefundsCount++
    //     }
    //   })
    // })

    return unsavedRefundsCount > 0;
  };

  const navigateToWalletBackupScreen = () =>
    navigation.navigate('TabsStack', {
      screen: 'SettingsStack',
      params: {
        screen: 'WalletBackup',
      },
    });
  // const navigateToSupportScreen = () =>
  //   navigation.navigate('TabsStack', {
  //     screen: 'SettingsStack',
  //     params: {
  //       screen: 'Support',
  //     },
  //   });

  const onChangeNetwork = async () => {
    setNetworkTypeIsChanging(true);
    toggleNetwork();
    setNetworkTypeIsChanging(false);
    setChangeNetworkModalIsVisible(false);

    navigation.navigate('TabsStack', {
      screen: 'BalanceStack',
      params: {
        screen: 'Balance',
      },
    });
  };

  const onTogglePinCode = () => {
    console.log('pin', pinCode);
    if (pinCode) {
      removePinCode();
    } else {
      showPinCodeScreen();
    }
  };

  const onLogout = async () => {
    setIsExiting(true);
    await logout();

    setTimeout(() => {
      setIsExiting(false);
      setLogoutConfirmationModalIsVisible(false);

      navigation.dispatch(
        StackActions.replace('OnboardingStack', {
          screen: 'Onboarding',
        }),
      );
    }, 150);
  };

  const renderChangeNetworkConfirmationModal = () => (
    <Modal
      isVisible={changeNetworkModalIsVisible}
      FooterComponent={
        <HStack space={2}>
          <Button
            flex={1}
            variant="ghost"
            onPress={() => setChangeNetworkModalIsVisible(false)}>
            {t('buttons.cancel')}
          </Button>
          <Button
            flex={1}
            isLoading={networkTypeIsChanging}
            onPress={onChangeNetwork}>
            {t('buttons.confirm')}
          </Button>
        </HStack>
      }
      onClosed={() => setChangeNetworkModalIsVisible(false)}>
      {hasUnsavedRefunds() ? (
        // <RefundsBackup />
        <View></View>
      ) : (
        <HStack justifyContent="center">
          <Text align="center">
            <Trans
              i18nKey="texts.changeNetworkConfirm"
              values={{
                network: network === 'mainnet' ? 'Testnet' : 'Mainnet',
              }}
              components={{ b: <Text fontWeight={700} /> }}
            />
          </Text>
        </HStack>
      )}
    </Modal>
  );

  const renderLogoutConfirmationModal = () => (
    <Modal
      isVisible={logoutConfirmationModalIsVisible}
      FooterComponent={
        <HStack space={2}>
          <Button
            flex={1}
            variant="ghost"
            onPress={() => setLogoutConfirmationModalIsVisible(false)}>
            {t('buttons.cancel')}
          </Button>
          <Button flex={1} isLoading={exiting} onPress={onLogout}>
            {t('buttons.logout')}
          </Button>
        </HStack>
      }
      onClosed={() => setLogoutConfirmationModalIsVisible(false)}>
      {hasUnsavedRefunds() ? (
        <View></View>
      ) : (
        //   <RefundsBackup />
        <HStack justifyContent="center">
          <Text align="center">{t('texts.logoutConfirm')}</Text>
        </HStack>
      )}
    </Modal>
  );

  return (
    <Screen
      header={<Header hasBackAction={false}>{t('titles.settings')}</Header>}>
      <ScreenContent>
        {renderLogoutConfirmationModal()}
        {renderChangeNetworkConfirmationModal()}

        <VStack space="xs">
          <Card pb="0">
            <Text textTransform="uppercase" color={labelColorToken}>
              {t('labels.security')}
            </Text>
            {mnemonic && (
              <>
                <Pressable onPress={navigateToWalletBackupScreen}>
                  <HStack
                    py="5"
                    alignItems="center"
                    justifyContent="space-between">
                    <Text>{t('buttons.backupPhrase')}</Text>
                    <Icon.ChevronRight color={iconColorToken} />
                  </HStack>
                </Pressable>
                <Divider />
              </>
            )}
            <HStack py="5" alignItems="center" justifyContent="space-between">
              <Text>{t('labels.pinCode')}</Text>
              <Switch value={!!pinCode} onToggle={onTogglePinCode} />
            </HStack>
          </Card>

          <Card pb="0">
            <Text textTransform="uppercase" color={labelColorToken}>
              {t('labels.general')}
            </Text>

            <Pressable onPress={() => setChangeNetworkModalIsVisible(true)}>
              <HStack py="5" alignItems="center" justifyContent="space-between">
                <Text>{t('labels.network')}</Text>
                <Text secondary textTransform="capitalize">
                  {network}
                </Text>
              </HStack>
            </Pressable>
            {/* <Divider />
            <Pressable onPress={navigateToSupportScreen}>
              <HStack py="5" alignItems="center" justifyContent="space-between">
                <Text textTx="buttons.support" />
                <Icon.ChevronRight
                  _light={{ color: "primaryAlpha.200" }}
                  _dark={{ color: "whiteAlpha.200" }}
                />
              </HStack>
            </Pressable> */}
          </Card>

          <Card py="0">
            <Pressable
              onPress={() => setLogoutConfirmationModalIsVisible(true)}>
              <HStack py="5" alignItems="center" justifyContent="space-between">
                <Text>{t('buttons.logout')}</Text>
                <Icon.Logout size={5} color={iconColorToken} />
              </HStack>
            </Pressable>
          </Card>

          <HStack justifyContent="center">
            <Text
              secondary
              opacity={
                40
              }>{`v${DeviceInfo.getVersion()} (${DeviceInfo.getBuildNumber()})`}</Text>
          </HStack>
        </VStack>
      </ScreenContent>
    </Screen>
  );
};
