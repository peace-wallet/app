import {
  Box,
  Button,
  Card,
  HStack,
  Header,
  Icon,
  IconButton,
  ScreenScrollViewContent,
  Toast,
  VStack,
} from '../components/base';
import Clipboard from '@react-native-clipboard/clipboard';
import { RouteProp, useNavigation, useRoute } from '@react-navigation/native';
import { StackNavigationProp } from '@react-navigation/stack';
import React, { useMemo } from 'react';
import { useTranslation } from 'react-i18next';
import { Linking, Share } from 'react-native';

import { Screen, TransactionDetails } from '../components/features';
import { SendNavigatorParamList } from '../navigators/SendSctack';
import { useAppStore } from '../stores';
import { getCoinByShardId } from '../utils/coins';

export type SentSuccessfullyScreenRouteProps = RouteProp<
  SendNavigatorParamList,
  'SentSuccessfully'
>;
export type SentSuccessfullyScreenNavigationProps = StackNavigationProp<
  SendNavigatorParamList,
  'SentSuccessfully'
>;

// export type SendCrossShardProcessingScreenParams = {
//   sourceShardId: number;
//   destinationShardId: number;
//   transactionHash: string;
// };

export type SentSuccessfullyScreenParams = {
  address: string;
  sourceAmount: number;
  destinationAmount: number;
  sourceShardId: number;
  destinationShardId?: number;
  transactionHash: string;
  transactionFee: number;
  exchangeAgentFee?: number;
  exchangeAgentUrl?: string;
};

export const SentSuccessfullyScreen = () => {
  const navigation = useNavigation();
  const { t } = useTranslation();

  const route = useRoute<SentSuccessfullyScreenRouteProps>();
  const {
    address,
    sourceShardId,
    destinationShardId,
    sourceAmount,
    destinationAmount,
    transactionFee,
    transactionHash,
    exchangeAgentFee,
    exchangeAgentUrl,
  } = route.params;

  const network = useAppStore(state => state.network);

  const coin = useMemo(() => getCoinByShardId(sourceShardId), [sourceShardId]);

  const url =
    coin.name === 'btc'
      ? `https://www.blockchain.com/en/search?search=${transactionHash}`
      : `https://explore.jax.net/shards/${sourceShardId}/transactions/${transactionHash}?network=${network}`;

  const onOpen = async () => {
    const supported = await Linking.canOpenURL(url);

    if (supported) {
      await Linking.openURL(url);
    } else {
      Toast.show({
        type: 'error',
        text1: "Don't know how to open this URL",
      });
    }
  };

  const onCopy = () => {
    Clipboard.setString(transactionHash);
    Toast.show({
      type: 'info',
      text1: t('notifications.copied'),
      text2: transactionHash,
    });
  };

  const onShare = async () => {
    try {
      const result = await Share.share({
        message: `Jax Explorer | ${transactionHash} \n\n${url}`,
      });
      if (result.action === Share.sharedAction) {
        Toast.show({
          type: 'info',
          text1: t('notifications.shared'),
          text2: transactionHash,
        });
      }
    } catch (error) {
      Toast.show({
        type: 'error',
        text1: t('notifications.error'),
        text2: error.message,
      });
    }
  };

  const onClose = () =>
    navigation.navigate('TabsStack', {
      screen: 'BalanceStack',
      params: {
        screen: 'Balance',
      },
    });

  return (
    <Screen
      header={
        <Header
          hasBackAction={false}
          rightAction={
            <IconButton
              variant="unstyled"
              icon={<Icon.Close />}
              onPress={onClose}
            />
          }>
          {t('titles.transactionCreated')}
        </Header>
      }>
      <ScreenScrollViewContent
        footer={
          <Button onPress={onOpen}>{t('buttons.openInExplorer')}</Button>
        }>
        <Card p="2">
          <VStack space={6}>
            <Box p="2">
              <TransactionDetails
                coin={coin}
                network={network}
                sourceAmount={sourceAmount}
                destinationAmount={destinationAmount}
                address={address}
                sourceShardId={sourceShardId}
                destinationShardId={destinationShardId}
                transactionFee={transactionFee}
                hash={transactionHash}
                exchangeAgentFee={exchangeAgentFee}
                exchangeAgentUrl={exchangeAgentUrl}
              />
            </Box>

            <HStack space="xs">
              <Button
                flex={1}
                variant="outline"
                borderRadius="md"
                endIcon={<Icon.Copy />}
                onPress={onCopy}>
                {t('buttons.copy')}
              </Button>
              <Button
                flex={1}
                variant="outline"
                borderRadius="md"
                endIcon={<Icon.Share />}
                onPress={onShare}>
                {t('buttons.share')}
              </Button>
            </HStack>
          </VStack>
        </Card>
      </ScreenScrollViewContent>
    </Screen>
  );
};
