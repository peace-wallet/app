import {
  Box,
  Button,
  Card,
  Center,
  Divider,
  HStack,
  Header,
  Icon,
  Input,
  Link,
  Modal,
  Pressable,
  RefreshControl,
  ScreenScrollViewContent,
  Select,
  Spinner,
  Text,
  Toast,
  VStack,
} from '../components/base';
import Clipboard from '@react-native-clipboard/clipboard';
import {
  RouteProp,
  StackActions,
  useNavigation,
  useRoute,
} from '@react-navigation/native';
import { StackNavigationProp } from '@react-navigation/stack';
import { useColorModeValue } from 'native-base';
import React, { useEffect, useMemo, useState } from 'react';
import { Controller, useForm } from 'react-hook-form';
import { Trans, useTranslation } from 'react-i18next';
import { useDebounce } from 'use-debounce';

import { Screen } from '../components/features';
import { FeeSelect, TransactionDetails } from '../components/features';
import {
  useBalances,
  useBtcBalance,
  useJaxBalances,
  useTransactionPublish,
  useTransactionsRaws,
  useUtxosAndFees,
} from '../hooks';
import { Balance, Fee } from '../models';
import { SendNavigatorParamList } from '../navigators/SendSctack';
import { useAppStore } from '../stores';
import { isValidAddress } from '../utils/address';
import { balanceByShardId, balancesByCoin } from '../utils/balance';
import { getCoinByShardId } from '../utils/coins';
import { CoinsFormatter } from '../utils/formatters';
import { prepareTransaction } from '../utils/transaction';

export type SendScreenRouteProps = RouteProp<SendNavigatorParamList, 'Send'>;
export type SendScreenNavigationProps = StackNavigationProp<
  SendNavigatorParamList,
  'Send'
>;

export type SendScreenParams = {
  address?: string;
  amount?: number;
  shardId?: number;
};

type FormData = {
  address: string;
  amount: number;
};

export const SendScreen = () => {
  const { t } = useTranslation();
  const navigation = useNavigation();

  const route = useRoute<SendScreenRouteProps>();
  const {
    address: initialAddress,
    amount: initialAmount,
    shardId: initialShardId,
  } = route.params ?? {};

  const shardLabelColorToken = useColorModeValue(
    'primaryAlpha.300',
    'whiteAlpha.300',
  );

  const {
    control,
    watch,
    setValue,
    handleSubmit,
    formState: { errors },
  } = useForm<FormData>({
    mode: 'all',
    shouldFocusError: true,
  });

  const amount = watch('amount');
  const address = watch('address');

  const [feeReloadLocked, setFeeReloadLocked] = useState(false);

  const [debouncedAmount] = useDebounce(amount, 200);

  const node = useAppStore(state => state.node);
  const network = useAppStore(state => state.network);
  const senderAddress = useAppStore(state => state.address);

  const [sourceShardId, setSourceShardId] = useState(undefined);
  const [destinationShardId, setDestinationShardId] = useState(undefined);

  const coin = useMemo(
    () => getCoinByShardId(initialShardId),
    [initialShardId],
  );

  // const { data: btcBalance } = useBtcBalance(senderAddress, network);
  const { data: jaxBalances } = useJaxBalances(senderAddress, network);

  const balances = useMemo(
    () => [
      // btcBalance,
      ...jaxBalances,
    ],
    [
      // btcBalance,
      jaxBalances,
    ],
  );

  const {
    data: utxosAndFeesDataLoaded,
    isFetching: utxosAndFeeIsLoading,
    isSuccess: utxosAndFeeIsSuccess,
  } = useUtxosAndFees(
    sourceShardId,
    CoinsFormatter.toSmallestUnits(coin.name, Number(debouncedAmount)),
    senderAddress,
    network,
    !errors.amount && !feeReloadLocked,
  );

  const utxosAndFeesData = useMemo(() => {
    return utxosAndFeesDataLoaded && !errors.amount
      ? utxosAndFeesDataLoaded
      : {
          code: 0,
          utxos: [],
          fees: [
            {
              name: 'slow',
              value: 0,
              maxAmount: 0,
            },
            {
              name: 'moderate',
              value: 0,
              maxAmount: 0,
            },
            {
              name: 'fast',
              value: 0,
              maxAmount: 0,
            },
          ] as Fee[],
        };
  }, [utxosAndFeesDataLoaded, errors.amount]);

  const {
    mutateAsync: loadTransactionsRaws,
    isLoading: isLoadingTransactionsRaws,
  } = useTransactionsRaws();

  const { mutateAsync: publishTransaction, isLoading: isPublishing } =
    useTransactionPublish();

  const balance = useMemo(
    () => balanceByShardId(sourceShardId, balances)?.balance ?? 0,
    [balances, sourceShardId],
  );

  const [selectedFeeName, setSelectedFeeName] = useState(
    utxosAndFeesData.fees[1].name,
  );
  const selectedFee = useMemo(() => {
    return utxosAndFeesData.fees.find(f => f.name === selectedFeeName);
  }, [selectedFeeName, utxosAndFeesData.fees]);

  const firstAvailableBalance = useMemo<Balance | undefined>(
    () =>
      balancesByCoin(coin, balances, {
        excludeEmpty: true,
      })[0],
    [balances, coin],
  );

  // const [refreshingExchangeAgents, setRefreshingExchangeAgents] =
  //   useState(false);

  const [confirmationModalIsVisible, setConfirmationModalIsVisible] =
    useState(false);
  const [maxAmountModalIsVisible, setMaxAmountModalIsVisible] = useState(false);
  const [tooManyUTXOsModalIsVisible, setTooManyUTXOsModalIsVisible] =
    useState(false);
  const [exchangeAgentsModalIsVisible, setExchangeAgentsModalIsVisible] =
    useState(false);

  const isRegularTransaction = sourceShardId === destinationShardId;

  const navigateToSendScanQRScreen = () => {
    // navigation.navigate('sendScanQR', {
    //   sendCoinName: coin.name,
    //   formIsDirty: Object.keys(dirtyFields).length > 0,
    //   cached: {
    //     coinName: coin.name,
    //     address: errors.address ? null : address,
    //     amount: errors.amount ? null : amount,
    //     shardID: destinationShardID,
    //   },
    // });
  };

  const onPasteAddressFromClipboard = async () => {
    const text = (await Clipboard.getString()).replace(/\n/g, '').trim();
    setValue('address', text, { shouldValidate: true, shouldDirty: true });
  };

  const createRegularTransaction = async () => {
    // const chunkSize = 64;

    // const chunks = [];

    // for (let i = 0; i < utxosAndFeesData.utxos.length; i += chunkSize) {
    //   const chunk = utxosAndFeesData.utxos
    //     .slice(i, i + chunkSize)
    //     .map((utxo) => utxo.hash);
    //   chunks.push(chunk);
    // }

    // const response = await Promise.all(
    //   chunks.map((chunk) =>
    //     loadTransactionsRaws({
    //       shardId: sourceShardId,
    //       hashes: chunk,
    //     })
    //   )
    // );

    // const raws = Object.assign({}, ...response);

    const raws = await loadTransactionsRaws({
      shardId: sourceShardId,
      hashes: utxosAndFeesData.utxos.map(utxo => utxo.hash),
    });

    let hex = '';

    try {
      hex = prepareTransaction(
        CoinsFormatter.toSmallestUnits(coin.name, amount),
        selectedFee.value,
        senderAddress,
        address,
        utxosAndFeesData.utxos,
        raws,
        node,
        network,
      );
    } catch (error) {
      Toast.show({
        type: 'error',
        text1: t('notifications.error'),
        text2: `prepareTransaction: ${error.message}`,
      });
    }

    const hash = await publishTransaction({ shardId: sourceShardId, hex });

    navigation.dispatch(
      StackActions.replace('SendStack', {
        screen: 'SentSuccessfully',
        params: {
          address,
          sourceShardId,
          sourceAmount: CoinsFormatter.toSmallestUnits(coin.name, amount),
          destinationAmount: CoinsFormatter.toSmallestUnits(coin.name, amount),
          transactionFee: selectedFee.value,
          transactionHash: hash,
        },
      }),
    );

    // historyStore.addOrUpdateLocal(
    //   sourceShardID,
    //   HistoryItemModel.create({
    //     address: address,
    //     sourceAmount:
    //       CoinsFormatter.toSmallestUnits(coin.name, amount) + fee,
    //     destinationAmount:
    //       CoinsFormatter.toSmallestUnits(coin.name, amount) + fee,
    //     transactionFee: fee,
    //     confirmations: 0,
    //     direction: 'out',
    //     sourceShardID: sourceShardID,
    //     destinationShardID: sourceShardID,
    //     timestamp: new Date().toUTCString(),
    //     transactionHash: hash,
    //     blockNumber: null,
    //     crossShardTemp: null,
    //   })
    // );
  };

  const createCrossShardTransaction = async () => {
    //   const tempTxHash = `${amount}:${sourceShardID}:${destinationShardID}:${address}:${selectedExchangeAgent.publicKey}`;
    //   const historyItem = HistoryItemModel.create({
    //     address,
    //     sourceAmount: CoinsFormatter.toSmallestUnits(coin.name, amount),
    //     destinationAmount:
    //       CoinsFormatter.toSmallestUnits(coin.name, amount) -
    //       selectedExchangeAgent?.proposal?.calculateFee(
    //         CoinsFormatter.toSmallestUnits(coin.name, amount)
    //       ),
    //     transactionFee: fee,
    //     exchangeAgentFee: selectedExchangeAgent?.proposal?.calculateFee(
    //       CoinsFormatter.toSmallestUnits(coin.name, amount)
    //     ),
    //     confirmations: 0,
    //     direction: 'out',
    //     sourceShardID,
    //     destinationShardID,
    //     timestamp: new Date().toUTCString(),
    //     transactionHash: tempTxHash,
    //     blockNumber: null,
    //     crossShardTemp: HistoryCrossShardTempModel.create({
    //       exchangeAgentUrl: selectedExchangeAgent.addresses[0].url,
    //       exchangeAgentPublicKey: selectedExchangeAgent.publicKey,
    //     }),
    //   });
    // setConfirmationModalIsVisible(false);
    // historyStore.addOrUpdateLocal(sourceShardID, historyItem);
    // await delay(150);
    // navigation.reset({
    //   routes: [
    //     {
    //       name: 'sendCrossShardProcessing',
    //       params: {
    //         sourceShardID,
    //         destinationShardID,
    //         transactionHash: tempTxHash,
    //       },
    //     },
    //   ],
    // });
  };

  const onSubmit = () =>
    isRegularTransaction
      ? setConfirmationModalIsVisible(true)
      : setExchangeAgentsModalIsVisible(true);

  const onConfirm = () =>
    isRegularTransaction
      ? createRegularTransaction()
      : createCrossShardTransaction();

  // const onRefreshExchangeAgents = async () => {
  //   setRefreshingExchangeAgents(true);
  //   await getExchangeAgents();
  //   setRefreshingExchangeAgents(false);
  // };

  // const getUTXOsAndFees = (amount: number | string) => {
  //   if (sourceShardID === null || sourceShardID === undefined) return;

  //   if (!amount || amount <= 0 || errors.amount !== undefined) return;

  //   setFeeIsLoading(true);
  //   transactionStore
  //     .getUTXOsAndFees(
  //       sourceShardID,
  //       walletStore.address,
  //       CoinsFormatter.toSmallestUnits(coin.name, Number(amount))
  //     )
  //     .then(() => {
  //       if (transactionStore.isError) {
  //         if (transactionStore.error.source?.code === 107) {
  //           Toast.show({
  //             type: 'info',
  //             text1: t('notifications.noFreeUTXOTitle'),
  //             text2: t('notifications.noFreeUTXODescription'),
  //             visibilityTime: 15000,
  //           });
  //         } else {
  //           Toast.show({
  //             type: 'error',
  //             text1: t('notifications.error'),
  //             text2: `${transactionStore.error.message}`,
  //           });
  //         }
  //       }

  //       setAmountIsEdited(false);
  //       setFeeIsLoading(false);
  //     });
  // };

  // const getExchangeAgents = async () => {
  //   if (
  //     sourceShardID === null ||
  //     sourceShardID === undefined ||
  //     destinationShardID === null ||
  //     destinationShardID === undefined
  //   )
  //     return;

  //   await exchangeAgentStore
  //     .getExchangeAgents(sourceShardID, destinationShardID)
  //     .then(() => {
  //       if (exchangeAgentStore.error) {
  //         Toast.show({
  //           type: 'error',
  //           text1: t('notifications.error'),
  //           text2: `${exchangeAgentStore.error.message}`,
  //         });
  //       }
  //     });
  // };

  const renderAddressCaption = () => {
    let text: string = null;

    switch (errors.address?.type) {
      case 'required':
        text = t('errors.addressIsRequired');
        break;
      case 'valid':
        text = t('errors.addressIsInvalid');
        break;
      default:
        text = null;
        break;
    }

    return (
      text && (
        <HStack px="3" alignItems="center" space={2}>
          <Icon.Info size={3.5} color="red.500" />
          <Text fontSize="sm" color="red.500">
            {text}
          </Text>
        </HStack>
      )
    );
  };

  const renderAmountCaption = () => {
    let text: string = null;

    if (balance === 0) {
      text = t('errors.notEnoughBalance');
    } else {
      switch (errors.amount?.type) {
        case 'required':
          text = t('errors.amountIsRequired');
          break;
        case 'valid':
          text = t('errors.amountIsInvalid');
          break;
        case 'min':
          text = t('errors.amountIsTooLow');
          break;
        case 'max':
          text = t('errors.amountIsTooHigh');
          break;
        default:
          text = null;
          break;
      }
    }

    return (
      text && (
        <HStack flex={1} px="4" mb="1" alignItems="center" space={2}>
          <Icon.Info size={3.5} color="red.500" />
          <Text fontSize="sm" color="red.500" flex={1}>
            {text}
          </Text>
        </HStack>
      )
    );
  };

  // const renderExchangeAgentListItem = (item: {
  //   item: ExchangeAgent;
  //   index;
  // }) => (
  //   <Pressable onPress={() => selectExchangeAgent(item.item)}>
  //     <Box
  //       _light={{
  //         bgColor:
  //           item.item.id === selectedExchangeAgent?.id
  //             ? 'primaryAlpha.50'
  //             : 'transparent',
  //       }}
  //       _dark={{
  //         bgColor:
  //           item.item.id === selectedExchangeAgent?.id
  //             ? 'whiteAlpha.50'
  //             : 'transparent',
  //       }}
  //     >
  //       <HStack
  //         space="sm"
  //         alignItems="center"
  //         justifyContent="space-between"
  //         px="4"
  //         py="3"
  //       >
  //         <Text flex={1} fontSize="sm">
  //           {item.item.addresses[0].url}
  //         </Text>
  //         <Text secondary>{`${t('labels.fee')} ${
  //           item.item.proposal
  //             ? CoinsFormatter.toBiggestUnits(
  //                 coin.name,
  //                 item.item.proposal?.calculateFee(
  //                   CoinsFormatter.toSmallestUnits(coin.name, amount)
  //                 )
  //               )
  //             : 'N/A'
  //         } ${coin.title}`}</Text>
  //       </HStack>
  //     </Box>
  //   </Pressable>
  // );

  const renderMaxAmountModal = () => {
    let action: 'edit' | 'accept' = 'edit';

    const handleClosed = () => {
      if (action === 'edit') {
        setValue('amount', '' as unknown as number);
      }

      if (action === 'accept') {
        setFeeReloadLocked(true);
        setValue(
          'amount',
          CoinsFormatter.toBiggestUnits(coin.name, selectedFee.maxAmount),
        );
      }

      setMaxAmountModalIsVisible(false);
    };

    return (
      <Modal
        isVisible={maxAmountModalIsVisible}
        onClosed={handleClosed}
        FooterComponent={
          <HStack space="xs">
            <Button
              flex={1}
              variant="ghost"
              onPress={() => {
                action = 'edit';
                setMaxAmountModalIsVisible(false);
              }}>
              {t('buttons.editAmount')}
            </Button>
            <Button
              flex={1}
              onPress={() => {
                action = 'accept';
                setMaxAmountModalIsVisible(false);
              }}>
              {t('buttons.accept')}
            </Button>
          </HStack>
        }>
        <Text align="center">
          <Trans
            i18nKey="texts.maximumAmount"
            values={{
              coin: coin.title,
              balance: CoinsFormatter.toBiggestUnits(coin.name, balance),
              fee: CoinsFormatter.toBiggestUnits(coin.name, selectedFee.value),
              maxAmount: CoinsFormatter.toBiggestUnits(
                coin.name,
                selectedFee.maxAmount,
              ),
            }}
            components={{ b: <Text fontWeight={700} /> }}
          />
        </Text>
      </Modal>
    );
  };

  const renderTooManyUTXOsModal = () => {
    let action: 'edit' | 'accept' = 'edit';

    const handleClosed = () => {
      if (action === 'edit') {
        setValue('amount', '' as unknown as number);
      }

      if (action === 'accept') {
        setFeeReloadLocked(true);
        setValue(
          'amount',
          CoinsFormatter.toBiggestUnits(coin.name, selectedFee.maxAmount),
        );
      }

      setTooManyUTXOsModalIsVisible(false);
    };

    return (
      <Modal
        isVisible={tooManyUTXOsModalIsVisible}
        onClosed={handleClosed}
        FooterComponent={
          <HStack space="xs">
            <Button
              flex={1}
              variant="ghost"
              onPress={() => {
                action = 'edit';
                setTooManyUTXOsModalIsVisible(false);
              }}>
              {t('buttons.editAmount')}
            </Button>
            <Button
              flex={1}
              onPress={() => {
                action = 'accept';
                setTooManyUTXOsModalIsVisible(false);
              }}>
              {t('buttons.accept')}
            </Button>
          </HStack>
        }>
        <Text align="center">
          <Trans
            i18nKey="texts.tooManyUTXOs"
            values={{
              coin: coin.title,
              balance: CoinsFormatter.toBiggestUnits(coin.name, balance),
              fee: CoinsFormatter.toBiggestUnits(coin.name, selectedFee.value),
              maxAmount: CoinsFormatter.toBiggestUnits(
                coin.name,
                selectedFee.maxAmount,
              ),
            }}
            components={{ b: <Text fontWeight={700} /> }}
          />
        </Text>
      </Modal>
    );
  };

  const renderConfirmationModal = () => (
    <Modal
      isVisible={confirmationModalIsVisible}
      HeaderComponent={
        <Text textTransform="uppercase">{t('titles.operationDetails')}</Text>
      }
      FooterComponent={
        <HStack space="xs">
          <Button
            flex={1}
            variant="ghost"
            onPress={() => setConfirmationModalIsVisible(false)}>
            {t(isRegularTransaction ? 'buttons.cancel' : 'buttons.back')}
          </Button>
          <Button
            flex={1}
            isLoading={isLoadingTransactionsRaws || isPublishing}
            onPress={onConfirm}>
            {t('buttons.confirm')}
          </Button>
        </HStack>
      }
      onClosed={() => setConfirmationModalIsVisible(false)}>
      <TransactionDetails
        coin={coin}
        network={network}
        sourceAmount={CoinsFormatter.toSmallestUnits(coin.name, amount)}
        // destinationAmount={
        //   CoinsFormatter.toSmallestUnits(coin.name, amount) -
        //   selectedExchangeAgent?.proposal?.calculateFee(
        //     CoinsFormatter.toSmallestUnits(coin.name, amount)
        //   )
        // }
        address={address}
        sourceShardId={sourceShardId}
        destinationShardId={destinationShardId}
        transactionFee={selectedFee.value}
        // exchangeAgentFee={selectedExchangeAgent?.proposal?.calculateFee(
        //   CoinsFormatter.toSmallestUnits(coin.name, amount)
        // )}
        // exchangeAgentUrl={selectedExchangeAgent?.addresses[0].url}
      />
    </Modal>
  );

  useEffect(() => {
    // if (
    //   !initialCoinNameIsValid ||
    //   !initialAmountIsValid ||
    //   !initialShardIDIsValid
    // ) {
    //   navigation.canGoBack() && navigation.goBack();

    //   if (walletStore.mnemonic || walletStore.privateKey || walletStore.wif) {
    //     Toast.show({
    //       type: 'error',
    //       text1: t('notifications.error'),
    //       text2: t('notifications.invalidQRCode'),
    //     });
    //   }

    //   return;
    // }

    if (initialAddress !== null && initialAddress !== undefined) {
      setValue('address', initialAddress, {
        shouldValidate: true,
      });
    }

    if (initialAmount !== null && initialAmount !== undefined) {
      setValue('amount', initialAmount, { shouldValidate: true });
    }

    if (
      initialShardId !== null &&
      initialShardId !== undefined &&
      coin.name === 'jax'
    ) {
      setSourceShardId(initialShardId);
      setDestinationShardId(initialShardId);
    } else {
      setSourceShardId(firstAvailableBalance?.shardId ?? coin?.shardId ?? 1);
      setDestinationShardId(
        firstAvailableBalance?.shardId ?? coin.shardId ?? 1,
      );
    }

    setSourceShardId(firstAvailableBalance?.shardId ?? coin.shardId ?? 1);
  }, [
    coin,
    firstAvailableBalance?.shardId,
    initialAddress,
    initialAmount,
    initialShardId,
    setValue,
  ]);

  useEffect(() => {
    if (
      CoinsFormatter.toSmallestUnits(coin.name, debouncedAmount) >
        selectedFee.maxAmount &&
      selectedFee.maxAmount > 0 &&
      !utxosAndFeeIsLoading &&
      utxosAndFeeIsSuccess
    ) {
      if (utxosAndFeesData.code !== 0) {
        setTooManyUTXOsModalIsVisible(true);
        return;
      }

      setMaxAmountModalIsVisible(true);
    }
  }, [
    utxosAndFeesData,
    debouncedAmount,
    selectedFee,
    utxosAndFeeIsSuccess,
    utxosAndFeeIsLoading,
    coin.name,
  ]);

  return (
    <Screen header={<Header>{`${t('titles.send')} ${coin.title}`}</Header>}>
      <ScreenScrollViewContent
        footer={
          <Button
            isDisabled={
              !selectedFee.value ||
              utxosAndFeeIsLoading ||
              !!errors.address ||
              !!errors.amount
            }
            onPress={handleSubmit(onSubmit)}>
            {t('buttons.send')}
          </Button>
        }>
        {/* {renderExchangeAgentsModal()} */}
        {renderConfirmationModal()}
        {renderMaxAmountModal()}
        {renderTooManyUTXOsModal()}

        <VStack space="xs">
          <Card error={!!errors.address}>
            <Controller
              control={control}
              render={({ field: { onChange, onBlur, value } }) => (
                <Input
                  variant="unstyled"
                  placeholder={t('placeholders.receiverAddress')}
                  value={value}
                  px="0"
                  py="2"
                  size="lg"
                  error={!!errors.address}
                  // InputRightElement={
                  //   <Pressable onPress={navigateToSendScanQRScreen}>
                  //     <Icon.ScanQR ml="2" />
                  //   </Pressable>
                  // }
                  onBlur={onBlur}
                  onChangeText={onChange}
                />
              )}
              name="address"
              rules={{
                required: true,
                validate: {
                  valid: value => isValidAddress(value, network),
                },
              }}
              defaultValue={initialAddress}
            />
          </Card>

          {renderAddressCaption()}

          <HStack justifyContent="center" mb="1">
            <Button
              size="md"
              variant="subtle"
              startIcon={<Icon.Paste />}
              onPress={onPasteAddressFromClipboard}>
              {t('buttons.paste')}
            </Button>
          </HStack>

          {coin.name === 'jax' && (
            <Card>
              <HStack space="xs" alignItems="center">
                <VStack flex={1}>
                  <Select
                    px="0"
                    py="2"
                    variant="unstyled"
                    dropdownCloseIcon={<Icon.ChevronDown />}
                    dropdownOpenIcon={<Icon.ChevronTop />}
                    selectedValue={sourceShardId?.toString()}
                    onValueChange={value => {
                      setSourceShardId(parseInt(value));
                    }}>
                    {balancesByCoin(coin, balances, { excludeEmpty: true }).map(
                      balanceByShard => (
                        <Select.Item
                          key={`select-shard-${balanceByShard.shardId}`}
                          label={`${t('labels.shard')} ${
                            balanceByShard.shardId
                          }`}
                          value={balanceByShard.shardId.toString()}
                        />
                      ),
                    )}
                  </Select>
                  <Text fontSize="xs" color={shardLabelColorToken}>
                    {t('labels.sourceShard')}
                  </Text>
                </VStack>
                <Divider height="100%" orientation="vertical" mr={1} />

                <VStack flex={1}>
                  <Select
                    px="0"
                    py="2"
                    variant="unstyled"
                    dropdownCloseIcon={<Icon.ChevronDown />}
                    dropdownOpenIcon={<Icon.ChevronTop />}
                    selectedValue={destinationShardId?.toString()}
                    onValueChange={value =>
                      setDestinationShardId(parseInt(value))
                    }>
                    {balancesByCoin(coin, balances).map(balanceByShard => (
                      <Select.Item
                        key={`destination-shard-${balanceByShard.shardId}`}
                        value={balanceByShard.shardId.toString()}
                        label={`${t('labels.shard')} ${balanceByShard.shardId}`}
                      />
                    ))}
                  </Select>
                  <Text fontSize="xs" color={shardLabelColorToken}>
                    {t('labels.destinationShard')}
                  </Text>
                </VStack>
              </HStack>
            </Card>
          )}

          <Card error={!!errors.amount}>
            <Controller
              control={control}
              render={({ field: { onChange, onBlur, value } }) => (
                <Input
                  variant="unstyled"
                  keyboardType="numeric"
                  placeholder={t('placeholders.enterAmount')}
                  value={value?.toString()}
                  px="0"
                  py="2"
                  size="lg"
                  error={!!errors.amount}
                  InputRightElement={
                    utxosAndFeeIsLoading ? (
                      <HStack alignItems="center">
                        <Spinner />
                      </HStack>
                    ) : (
                      <Button
                        variant="ghost"
                        size="sm"
                        py={1}
                        onPress={() => {
                          setValue(
                            'amount',
                            CoinsFormatter.toBiggestUnits(coin.name, balance),
                            {
                              shouldValidate: true,
                            },
                          );
                          setFeeReloadLocked(false);
                        }}>
                        <Text fontSize="md">{t('buttons.max')}</Text>
                      </Button>
                    )
                  }
                  onBlur={onBlur}
                  onChangeText={value => {
                    setFeeReloadLocked(false);
                    onChange(value.replace(',', '.'));
                  }}
                />
              )}
              name="amount"
              rules={{
                required: true,
                min: 0.00000001,
                max: CoinsFormatter.toBiggestUnits(coin.name, balance),
                validate: {
                  valid: value =>
                    (coin.name === 'jax'
                      ? /^\d+(\.\d{1,4})?$/
                      : /^\d+(\.\d{1,8})?$/
                    ).test(value.toString()),
                },
              }}
              defaultValue={initialAmount}
            />
            <HStack alignItems="center" mt="1" space={1}>
              <Text
                fontSize="xs"
                color={useColorModeValue('primaryAlpha.300', 'whiteAlpha.300')}>
                {t('texts.availableBalance', {
                  balance: CoinsFormatter.toBiggestUnits(coin.name, balance),
                  coin: coin.title,
                })}
              </Text>
            </HStack>
          </Card>

          {renderAmountCaption()}

          <Card p="0" mb="2">
            <FeeSelect
              fees={utxosAndFeesData.fees}
              value={selectedFee}
              coin={coin}
              onChange={fee => setSelectedFeeName(fee.name)}
            />
          </Card>
        </VStack>
      </ScreenScrollViewContent>
    </Screen>
  );
};
