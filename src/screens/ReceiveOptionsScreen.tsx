import {
  Button,
  Card,
  HStack,
  Header,
  Icon,
  Input,
  ScreenScrollViewContent,
  Select,
  Text,
  VStack,
} from '../components/base';
import { RouteProp, useNavigation, useRoute } from '@react-navigation/native';
import { StackNavigationProp } from '@react-navigation/stack';
import { useColorModeValue } from 'native-base';
import React, { useEffect, useMemo, useState } from 'react';
import { Controller, useForm } from 'react-hook-form';
import { useTranslation } from 'react-i18next';

import { Screen } from '../components/features';
import { useBtcBalance, useJaxBalances } from '../hooks';
import { ReceiveNavigatorParamList } from '../navigators/ReceiveStack';
import { useAppStore } from '../stores';
import { balancesByCoin } from '../utils/balance';
import { getCoinByShardId } from '../utils/coins';

export type ReceiveOptionsScreenRouteProps = RouteProp<
  ReceiveNavigatorParamList,
  'ReceiveOptions'
>;
export type ReceiveOptionsScreenNavigationProps = StackNavigationProp<
  ReceiveNavigatorParamList,
  'ReceiveOptions'
>;

export type ReceiveOptionsScreenParams = {
  amount?: string;
  shardId: number;
};

export const ReceiveOptionsScreen = () => {
  const route = useRoute<ReceiveOptionsScreenRouteProps>();
  const { amount: initialAmount, shardId: initialShardId } = route.params ?? {};

  const shardLabelColorToken = useColorModeValue(
    'primaryAlpha.300',
    'whiteAlpha.300',
  );

  const { t } = useTranslation();
  const navigation = useNavigation();

  const {
    control,
    watch,
    handleSubmit,
    setValue,
    formState: { errors },
  } = useForm({
    mode: 'onChange',
    shouldFocusError: true,
    shouldUnregister: false,
  });

  const amount = watch('amount');

  const network = useAppStore(state => state.network);
  const address = useAppStore(state => state.address);

  const coin = useMemo(
    () => getCoinByShardId(initialShardId),
    [initialShardId],
  );

  const { data: btcBalance } = useBtcBalance(address, network);
  const { data: jaxBalances } = useJaxBalances(address, network);

  const balances = useMemo(
    () => [btcBalance, ...jaxBalances],
    [btcBalance, jaxBalances],
  );

  const [shardId, setShardId] = useState(initialShardId ?? null);

  const onSubmit = () => {
    navigation.navigate('TabsStack', {
      screen: 'BalanceStack',
      params: {
        screen: 'ReceiveStack',
        params: {
          screen: 'Receive',
          params: {
            amount,
            shardId,
          },
        },
      },
    });
  };

  const renderAmountCaption = () => {
    let text: string = null;

    switch (errors.amount?.type) {
      case 'valid':
        text = t('errors.amountIsInvalid');
        break;
      case 'min':
        text = t('errors.amountIsTooLow');
        break;
      default:
        text = null;
        break;
    }

    return (
      text && (
        <HStack flex={1} px="4" mb="1" alignItems="center" space={2}>
          <Icon.Info size={3.5} color="red.500" />
          <Text fontSize="sm" color="red.500" flex={1}>
            {text}
          </Text>
        </HStack>
      )
    );
  };

  useEffect(() => {
    setValue(
      'amount',
      initialAmount && isNaN(parseFloat(initialAmount))
        ? undefined
        : initialAmount,
    );
  }, [initialAmount, setValue]);

  return (
    <Screen
      header={<Header>{`${t('titles.receive')} ${coin.title}`}</Header>}
      unsafeBottom>
      <ScreenScrollViewContent
        footer={
          <VStack space="md">
            <Button onPress={handleSubmit(onSubmit)}>
              {t('buttons.accept')}
            </Button>
          </VStack>
        }>
        <VStack space="xs" pb="2">
          <Card>
            <VStack flex={1}>
              <Controller
                control={control}
                render={({ field: { onChange, onBlur, value } }) => (
                  <Input
                    variant="unstyled"
                    keyboardType="numeric"
                    placeholder={t('placeholders.enterAmount')}
                    value={value}
                    px="0"
                    py="2"
                    size="lg"
                    error={!!errors.amount}
                    onBlur={onBlur}
                    onChangeText={value => onChange(value.replace(',', '.'))}
                  />
                )}
                name="amount"
                rules={{
                  required: false,
                  min: 0.00000001,
                  validate: {
                    valid: value =>
                      value && value.length > 0
                        ? (coin.name === 'jax'
                            ? /^\d+(\.\d{1,4})?$/
                            : /^\d+(\.\d{1,8})?$/
                          ).test(value)
                        : true,
                  },
                }}
              />
              <Text fontSize="xs" color={shardLabelColorToken}>
                {t('labels.amount')}
              </Text>
            </VStack>
          </Card>

          {renderAmountCaption()}

          {coin.name === 'jax' && (
            <Card>
              <VStack flex={1}>
                <Select
                  px="0"
                  py="2"
                  variant="unstyled"
                  placeholder={t('placeholders.selectShard')}
                  dropdownCloseIcon={<Icon.ChevronDown />}
                  dropdownOpenIcon={<Icon.ChevronTop />}
                  selectedValue={shardId?.toString()}
                  defaultValue={initialShardId?.toString()}
                  onValueChange={value => setShardId(parseInt(value))}>
                  {balancesByCoin(coin, balances).map(balanceByShard => (
                    <Select.Item
                      key={`select-shard-${balanceByShard.shardId}`}
                      label={`${t('labels.shard')} ${balanceByShard.shardId}`}
                      value={balanceByShard.shardId.toString()}
                    />
                  ))}
                </Select>
                <Text fontSize="xs" color={shardLabelColorToken}>
                  {t('labels.shard')}
                </Text>
              </VStack>
            </Card>
          )}
        </VStack>
      </ScreenScrollViewContent>
    </Screen>
  );
};
