import { theme } from '../theme';
import {
  Box,
  Button,
  Flex,
  HStack,
  Screen,
  ScreenContent,
  Spacer,
  Text,
  VStack,
} from '../components/base';
import { useNavigation } from '@react-navigation/native';
import { useColorModeValue } from 'native-base';
import React, { useRef, useState } from 'react';
import { useTranslation } from 'react-i18next';
import {
  Dimensions,
  NativeScrollEvent,
  NativeSyntheticEvent,
  ScrollView,
  StyleSheet,
  View,
} from 'react-native';

import FastWithdrawal from '../assets/images/onboarding/fast-withdrawal.svg';
import SecureImage from '../assets/images/onboarding/secure.svg';
import WorldImage from '../assets/images/onboarding/world.svg';

type OnboardingPage = {
  title: string;
  image: React.ReactNode;
};

export const OnboardingScreen = () => {
  const navigation = useNavigation();
  const { t } = useTranslation();

  const { width } = Dimensions.get('window');

  const activeDotColorToken = useColorModeValue('primary.500', 'primary.500');
  const inactiveDotColorToken = useColorModeValue(
    'primaryAlpha.200',
    'whiteAlpha.200',
  );

  const carouselRef = useRef(null);
  const [selectedOnboardingPageIndex, setSelectedOnboardingPageIndex] =
    useState(0);

  const navigateToCreateWalletScreen = () => {
    navigation.navigate('OnboardingStack', {
      screen: 'WalletStack',
      params: {
        screen: 'WalletCreate',
      },
    });
  };

  const navigateToRestoreWalletByMnemonicScreen = () =>
    navigation.navigate('OnboardingStack', {
      screen: 'WalletStack',
      params: {
        screen: 'WalletRestoreByMnemonic',
      },
    });

  const onboardingPages: Array<OnboardingPage> = [
    {
      image: <WorldImage />,
      title: t('texts.onboardingPage1'),
    },
    {
      image: <FastWithdrawal />,
      title: t('texts.onboardingPage2'),
    },
    {
      image: <SecureImage />,
      title: t('texts.onboardingPage3'),
    },
  ];

  const handleScrollEnd = (event: NativeSyntheticEvent<NativeScrollEvent>) => {
    const { x } = event.nativeEvent.contentOffset;
    const indexOfNextScreen = Math.round(x / width);

    setSelectedOnboardingPageIndex(indexOfNextScreen);
  };

  const renderCarouselItem = (item: OnboardingPage) => (
    <VStack
      key={`onboarding-item-${item.title}`}
      flex={1}
      alignItems="center"
      space="2xl"
      width={width}>
      <Spacer />
      {item.image}
      <HStack px="2">
        <Text align="center" fontSize={28} lineHeight="36px">
          {item.title}
        </Text>
      </HStack>
      <Spacer />
    </VStack>
  );

  const renderCarousel = () => (
    <ScrollView
      ref={carouselRef}
      style={{
        width: '100%',
        height: '100%',
        flex: 1,
      }}
      horizontal={true}
      scrollEventThrottle={16}
      showsHorizontalScrollIndicator={false}
      pagingEnabled={true}
      onMomentumScrollEnd={handleScrollEnd}>
      {onboardingPages.map(renderCarouselItem)}
    </ScrollView>
  );

  const renderCarouselPagination = () => (
    <HStack alignItems="center" justifyContent="center" space="sm">
      {onboardingPages.map((_, index) => (
        <Box
          key={`dot-${index}`}
          w={'16px'}
          h={'5px'}
          rounded="xs"
          bgColor={
            selectedOnboardingPageIndex === index
              ? activeDotColorToken
              : inactiveDotColorToken
          }
        />
      ))}
    </HStack>
  );

  return (
    <Screen testID="onboarding-screen">
      <ScreenContent>
        <VStack space="md" flex={1}>
          <Flex flex={3} style={styles.carousel}>
            {renderCarousel()}
          </Flex>

          <Box py="8">{renderCarouselPagination()}</Box> 

          <HStack space="xs">
            <Button
              flex={1}
              variant="outline"
              borderRadius="md"
              _light={{ backgroundColor: 'white' }}
              onPress={navigateToRestoreWalletByMnemonicScreen}>
              {t('buttons.restoreWallet')}
            </Button>
            <Button flex={1} onPress={navigateToCreateWalletScreen}>
              {t('buttons.createWallet')}
            </Button>
          </HStack>
        </VStack>
      </ScreenContent>
    </Screen>
  );
};

const styles = StyleSheet.create({
  carousel: {
    marginHorizontal: -theme.space[2],
  },
});
