import {
  Button,
  Card,
  HStack,
  Header,
  Icon,
  IconButton,
  ScreenScrollViewContent,
  Text,
  Toast,
  VStack,
} from '../components/base';
import Clipboard from '@react-native-clipboard/clipboard';
import { useNavigation } from '@react-navigation/core';
import { RouteProp, useRoute } from '@react-navigation/native';
import { StackNavigationProp } from '@react-navigation/stack';
import { useColorModeValue, useToken } from 'native-base';
import numeral from 'numeral';
import React, { useEffect, useMemo, useState } from 'react';
import { useTranslation } from 'react-i18next';
import { Dimensions, Share } from 'react-native';
import QRCode from 'react-native-qrcode-svg';

import { Screen } from '../components/features';
import { ReceiveNavigatorParamList } from '../navigators/ReceiveStack';
import { useAppStore } from '../stores';
import { getCoinByShardId } from '../utils/coins';

export type ReceiveScreenRouteProps = RouteProp<
  ReceiveNavigatorParamList,
  'Receive'
>;
export type ReceiveScreenNavigationProps = StackNavigationProp<
  ReceiveNavigatorParamList,
  'Receive'
>;

export type ReceiveScreenParams = {
  amount?: string;
  shardId: number;
};

export const ReceiveScreen = () => {
  const route = useRoute<ReceiveScreenRouteProps>();

  const { amount: initialAmount, shardId: initialShardId } = route.params ?? {};

  const { t } = useTranslation();
  const navigation = useNavigation();

  const network = useAppStore(state => state.network);
  const address = useAppStore(state => state.address);

  const coin = useMemo(
    () => getCoinByShardId(initialShardId),
    [initialShardId],
  );

  const qrCodeColor = useToken(
    'colors',
    useColorModeValue('primary.500', 'white'),
  );
  const qrCodeBackgroundColor = useToken(
    'colors',
    useColorModeValue('white', 'darkGray.500'),
  );

  const [amount, setAmount] = useState(null);
  const [shardId, setShardId] = useState(null);

  const url = `https://explore.jax.net/shards/addresses/${address}?network=${network}`;

  let qrData = `peacewallet://send/${coin.name}/${address}`;

  if (amount) {
    qrData += `/${amount}`;
  }

  if (shardId) {
    qrData += `/${shardId}`;
  }

  const onNavigateToEditOptions = async () => {
    navigation.navigate('TabsStack', {
      screen: 'BalanceStack',
      params: {
        screen: 'ReceiveStack',
        params: {
          screen: 'ReceiveOptions',
          params: {
            amount,
            shardId: shardId ?? initialShardId,
          },
        },
      },
    });
  };

  const onCopy = () => {
    Clipboard.setString(address);
    Toast.show({
      type: 'info',
      text1: t('notifications.copied'),
      text2: address,
    });
  };

  const onShare = async () => {
    try {
      const result = await Share.share({
        message: `Jax Explorer | ${address} \n\n${url}`,
      });
      if (result.action === Share.sharedAction) {
        Toast.show({
          type: 'info',
          text1: t('notifications.shared'),
          text2: address,
        });
      }
    } catch (error) {
      console.log('error', error);
    }
  };

  const onClear = () => {
    setAmount(null);
    setShardId(null);
  };

  useEffect(() => {
    setAmount(isNaN(parseFloat(initialAmount)) ? undefined : initialAmount);

    if (coin.name === 'jax') {
      setShardId(initialShardId);
    }
  }, [coin, initialAmount, initialShardId]);

  return (
    <Screen
      header={
        <Header
          rightAction={
            (amount || shardId) && (
              <IconButton
                variant="unstyled"
                icon={<Icon.Close />}
                onPress={onClear}
              />
            )
          }>{`${t('titles.receive')} ${coin.title}`}</Header>
      }
      unsafeBottom>
      <ScreenScrollViewContent
        footer={
          <VStack space="md">
            <Button onPress={onNavigateToEditOptions}>
              {t('buttons.createInvoice')}
            </Button>
          </VStack>
        }>
        <VStack space="xs" pb="2">
          {(amount || shardId) && (
            <Card p="4">
              <HStack>
                {amount && (
                  <VStack flex={1}>
                    <HStack space={1}>
                      <Text fontSize={28} color={`coins.${coin.name}.500`}>{`${
                        amount > 10000
                          ? numeral(amount).format('0.00a')
                          : amount
                      }`}</Text>
                    </HStack>
                    <Text secondary>{t('labels.amount')}</Text>
                  </VStack>
                )}
                {shardId && (
                  <VStack flex={1}>
                    <HStack space={1}>
                      <Text fontSize={28}>{`${t(
                        'labels.shard',
                      )} ${shardId}`}</Text>
                    </HStack>
                    <Text secondary>{t('labels.shard')}</Text>
                  </VStack>
                )}
              </HStack>
            </Card>
          )}

          <Card p="2">
            <VStack space={8}>
              <HStack px="2" pt="1">
                <Text secondary>{t('labels.qrCode')}</Text>
              </HStack>

              <HStack alignItems="center" justifyContent="center">
                <QRCode
                  color={qrCodeColor}
                  backgroundColor={qrCodeBackgroundColor}
                  size={Dimensions.get('screen').width * 0.6}
                  value={qrData}
                />
              </HStack>

              <HStack px="2" justifyContent="center">
                <Text secondary>{address}</Text>
              </HStack>

              <HStack space="xs">
                <Button
                  flex={1}
                  variant="outline"
                  borderRadius="md"
                  endIcon={<Icon.Copy />}
                  onPress={onCopy}>
                  {t('buttons.copy')}
                </Button>
                <Button
                  flex={1}
                  variant="outline"
                  borderRadius="md"
                  endIcon={<Icon.Share />}
                  onPress={onShare}>
                  {t('buttons.share')}
                </Button>
              </HStack>
            </VStack>
          </Card>
        </VStack>
      </ScreenScrollViewContent>
    </Screen>
  );
};
