import i18n from 'i18next';
import moment from 'moment';
import 'moment/locale/ru';
import 'moment/locale/uk';
import numeral from 'numeral';
import 'numeral/locales/ru';
import 'numeral/locales/uk-ua';
import { initReactI18next } from 'react-i18next';
import { NativeModules, Platform } from 'react-native';

import en from './locales/en.json';
import ru from './locales/ru.json';
import uk from './locales/uk.json';

const supportedLngs = ['en', 'ru', 'uk'];

export const defaultNS = 'translation';
export const resources = {
  en: { translation: en },
  ru: { translation: ru },
  uk: { translation: uk },
} as const;

i18n.use(initReactI18next).init({
  debug: process.env.NODE_ENV !== 'production',
  compatibilityJSON: 'v3',
  defaultNS,
  resources,

  fallbackLng: 'en',
  interpolation: {
    escapeValue: false,
  },
  react: { useSuspense: false },
});

i18n.on('languageChanged', (lng) => {
  const formattedLng = lng.split('-')[0];

  if (lng && supportedLngs.includes(formattedLng)) {
    numeral.locale(formattedLng === 'uk' ? 'uk-ua' : formattedLng);
    moment.locale(i18n.language);
  } else {
    numeral.locale('en');
  }
});

const deviceLanguage =
  Platform.OS === 'ios'
    ? NativeModules.SettingsManager.settings.AppleLocale ||
      NativeModules.SettingsManager.settings.AppleLanguages[0]
    : NativeModules.I18nManager.localeIdentifier;

i18n.changeLanguage(deviceLanguage.replace('_', '-'));

export default i18n;
