import { BaseRequestParams } from '../config';

export interface BalancesRequestParams extends BaseRequestParams {
  address: string;
}

export type BalancesResponse = {
  data: {
    shards: {
      shardID: number;
      balance: number;
      balanceLocked: number;
    }[];
  };
};
