import { AxiosResponse } from 'axios';
import { api } from '../config';
import * as Types from './balances-types';

/* ========================================================================== */
/*                                  Balances                                  */
/* ========================================================================== */

export const getBalances = async ({
  address,
  signal,
}: Types.BalancesRequestParams) => {
  // const btcResponse = await api.get(
  //   `/btc/v1/addresses/${address}/balance/`,
  //   {
  //     timeout: 60000,
  //     signal,
  //   }
  // );
  const jaxResponse: AxiosResponse<Types.BalancesResponse> = await api.get(
    `/jax/v1/addresses/${address}/balances/`,
    { signal },
  );

  // const btcBalance = {
  //   shardId: Math.pow(2, 32) - 1,
  //   ...btcResponse.data.data,
  //   balanceLocked: 0,
  // };

  const jaxData = jaxResponse.data.data.shards;

  const jaxBalance = jaxData.map(balance => ({
    shardId: balance.shardID,
    balance: balance?.balance,
    balanceLocked: balance?.balanceLocked,
  }));

  return [
    // btcBalance,
    ...jaxBalance,
  ];
};

/* ========================================================================== */
/*                                 BTC Balance                                */
/* ========================================================================== */

export const getBtcBalance = async (address: string, signal?: AbortSignal) => {
  const response = await api.get(
    `/btc/v1/addresses/1HtvTpVUVZZkw1SqEbVxGZyQwRBzXc6S52/balance/`,
    {
      timeout: 60000,
      signal,
    },
  );

  const data = response.data.data.balance;

  return data;
};

/* ========================================================================== */
/*                             JAX / JAX Balances                             */
/* ========================================================================== */

export const getJaxBalances = async ({
  address,
  signal,
}: Types.BalancesRequestParams) => {
  const response = await api.get(`/v1/addresses/${address}/balances/`, {
    signal,
  });

  const data = response.data.data.shards;

  return data;
};
