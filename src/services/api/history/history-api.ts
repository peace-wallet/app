import { getCoinByShardId } from '../../../utils/coins';
import { HistoryByShard } from './../../../models';
import { api } from '../config';
import * as Types from './history-types';

export const getHistory = async ({
  address,
  shardId,
  offset,
  limit,
  signal,
}: Types.HistoryRequestParams) => {
  const coin = getCoinByShardId(shardId);

  const url =
    coin.name === 'btc'
      ? `/btc/v1/addresses/1KKV9LqB2DpLhSU7fXY35vp2iitkR8DzN/history/?offset=${offset}&limit=${limit}`
      : `/v2/addresses/${address}/history/?offset=${offset}&limit=${limit}$shard=${shardId}`;

  const response = await api.get(url, { timeout: 60000, signal });

  const data = {
    shardId,
    items: response.data.data,
    loadedAll: response.data.data.length < limit,
  } as HistoryByShard;

  return data;
};
