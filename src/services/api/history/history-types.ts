import { BaseRequestParams } from '../config';

export interface HistoryRequestParams extends BaseRequestParams {
  address: string;
  shardId: number;
  offset: number;
  limit: number;
}

export type HistoryResponse = {
  data: {
    shards: {
      shardID: number;
      balance: number;
      balanceLocked: number;
    }[];
  };
};
