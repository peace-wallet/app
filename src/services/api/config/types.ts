export type BaseError = {
  code: number;
  message: string;
};

export type BaseRequestParams = {
  signal?: AbortSignal;
};
