import axios from 'axios';
// import { setupCache } from 'axios-cache-adapter';
import Config from 'react-native-config';

// const cacheAdapted = setupCache({
//   readHeaders: true,
// });

const api = axios.create({
  ...axios.defaults,
  baseURL: Config.MAINNET_API_URL,
  timeout: 10000,
  headers: {
    Accept: 'application/json',
    'Content-Type': 'application/json',
  },
  withCredentials: true,
  // adapter: cacheAdapted.adapter,
});

export { api };
