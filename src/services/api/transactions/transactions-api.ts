import { getCoinByShardId } from '../../../utils/coins';
import { api } from '../config';

/* ========================================================================== */
/*                              Transactions Raw                              */
/* ========================================================================== */

export const getTransactionsRaw = async (shardId: number, hashes: string[]) => {
  const coin = getCoinByShardId(shardId);

  const url =
    coin.name === 'btc'
      ? `/btc/v1/transactions/?hash=${hashes.join('&hash=')}`
      : `/v1/shards/${shardId}/transactions/?hash=${hashes.join('&hash=')}`;

  const response = await api.get(url, { timeout: 60000 });

  const data = response.data.data;

  return data;
};

/* ========================================================================== */
/*                             Publish Transaction                            */
/* ========================================================================== */

export const publishTransaction = async (
  shardId: number,
  transaction: string,
) => {
  const coin = getCoinByShardId(shardId);

  const url =
    coin.name === 'btc'
      ? `/btc/v1/transactions/publish/`
      : `/v1/shards/${shardId}/transactions/publish/`;

  const response = await api.post(
    url,
    {
      transaction,
    },
    { timeout: 60000 },
  );

  const data = response.data.data.hash;

  return data;
};
