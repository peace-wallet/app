import { getCoinByShardId } from '../../../utils/coins';
import { UtxosAndFees } from './../../../models';
import { api } from '../config';

export type UtxosAndFeesResponse = UtxosAndFees & {
  code: number;
};

export const getUtxosAndFees = async (
  shardId: number,
  amount: number,
  address: string,
) => {
  const coin = getCoinByShardId(shardId);

  const url =
    coin.name === 'btc'
      ? `/btc/v1/addresses/${address}/utxos/?amount=${amount}`
      : `/v1/shards/${shardId}/addresses/${address}/utxos/?amount=${amount}`;

  const response = await api.get(url, { timeout: 60000 });

  const data = response.data.data;

  const formattedData: UtxosAndFeesResponse = {
    code: response.data.code,
    utxos: data.utxos,
    fees: Object.keys(data.fees).map(key => ({
      name: key as 'fast' | 'moderate' | 'slow',
      value: data.fees[key].fee,
      maxAmount: data.fees[key].maxAmount,
    })),
  };

  return formattedData;
};
