import { QueryClient } from '@tanstack/react-query';

const queryClient = new QueryClient();

class QueryError {
  code: number;
  message: string;
  meta?: unknown;

  constructor(code: number, message: string, meta?: unknown) {
    this.code = code;
    this.message = message;
    this.meta = meta;
  }
}

export { queryClient, QueryError };
