import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { NavigatorScreenParams } from '@react-navigation/native';
import React from 'react';

import { BottomTabBar } from '../components/base';
import {
  BalanceNavigatorParamList,
  BalanceStackNavigator,
} from './BalanceStack';
import {
  SettingsNavigatorParamList,
  SettingsStackNavigator,
} from './SettingsStack';

export type TabsStackParamList = {
  BalanceStack: NavigatorScreenParams<BalanceNavigatorParamList>;
  SettingsStack: NavigatorScreenParams<SettingsNavigatorParamList>;
};

const Tab = createBottomTabNavigator<TabsStackParamList>();

export const TabsStackNavigator = () => {
  return (
    <Tab.Navigator
      screenOptions={{
        headerShown: false,
      }}
      tabBar={props => <BottomTabBar {...props} />}>
      <Tab.Screen
        name="BalanceStack"
        component={BalanceStackNavigator}
        options={{ tabBarLabel: 'tabs.balance' }}
      />
      <Tab.Screen
        name="SettingsStack"
        component={SettingsStackNavigator}
        options={{ tabBarLabel: 'tabs.settings' }}
      />
    </Tab.Navigator>
  );
};
