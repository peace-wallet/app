import { createNativeStackNavigator } from '@react-navigation/native-stack';
import React from 'react';

import { SettingsScreen, WalletBackupScreen } from '../screens';

export type SettingsNavigatorParamList = {
  Settings: undefined;
  WalletBackup: undefined;
  Support: undefined;
};

const Stack = createNativeStackNavigator<SettingsNavigatorParamList>();

export const SettingsStackNavigator = () => {
  return (
    <Stack.Navigator
      screenOptions={{
        headerShown: false,
      }}
    >
      <Stack.Screen name="Settings" component={SettingsScreen} />
      <Stack.Screen name="WalletBackup" component={WalletBackupScreen} />
      {/* <Stack.Screen name="Support" component={SupportScreen} /> */}
    </Stack.Navigator>
  );
};
