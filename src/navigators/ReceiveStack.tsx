import { createNativeStackNavigator } from '@react-navigation/native-stack';
import React from 'react';

import {
  ReceiveOptionsScreen,
  ReceiveOptionsScreenParams,
  ReceiveScreen,
  ReceiveScreenParams,
} from '../screens';

export type ReceiveNavigatorParamList = {
  Receive: ReceiveScreenParams;
  ReceiveOptions: ReceiveOptionsScreenParams;
};

const Stack = createNativeStackNavigator<ReceiveNavigatorParamList>();

export const ReceiveStackNavigator = () => {
  return (
    <Stack.Navigator
      screenOptions={{
        headerShown: false,
      }}>
      <Stack.Screen name="Receive" component={ReceiveScreen} />
      <Stack.Screen name="ReceiveOptions" component={ReceiveOptionsScreen} />
    </Stack.Navigator>
  );
};
