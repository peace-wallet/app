import { NavigatorScreenParams } from '@react-navigation/native';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import React from 'react';

import { OnboardingScreen } from '../screens';
import { WalletStackNavigator, WalletStackParamList } from './WalletStack';

export type OnboardingStackParamList = {
  Onboarding: undefined;
  WalletStack: NavigatorScreenParams<WalletStackParamList>;
};

const Stack = createNativeStackNavigator<OnboardingStackParamList>();

export const OnboardingStackNavigator = () => {
  return (
    <Stack.Navigator
      screenOptions={{
        headerShown: false,
      }}
    >
      <Stack.Screen name="Onboarding" component={OnboardingScreen} />
      <Stack.Screen name="WalletStack" component={WalletStackNavigator} />
    </Stack.Navigator>
  );
};
