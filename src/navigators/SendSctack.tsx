import { createNativeStackNavigator } from '@react-navigation/native-stack';
import React from 'react';

import {
  SendScreen,
  SendScreenParams,
  SentSuccessfullyScreen,
  SentSuccessfullyScreenParams,
} from '../screens';

export type SendNavigatorParamList = {
  Send: SendScreenParams;
  SentSuccessfully: SentSuccessfullyScreenParams;
  // sendScanQR: SendScanQRScreenParams
  // sendCrossShardRefund: SendCrossShardRefundScreenParams
  // sendCrossShardProcessing: SendCrossShardProcessingScreenParams
};

const Stack = createNativeStackNavigator<SendNavigatorParamList>();

export const SendStackNavigator = () => {
  return (
    <Stack.Navigator
      screenOptions={{
        headerShown: false,
      }}>
      <Stack.Screen name="Send" component={SendScreen} />
      <Stack.Screen
        name="SentSuccessfully"
        component={SentSuccessfullyScreen}
      />
      {/* <Stack.Screen name="sendScanQR" component={SendScanQRScreen} /> */}
      {/* <Stack.Screen name="sendCrossShardRefund" component={SentCrossShardRefundScreen} /> */}
      {/* <Stack.Screen name="sendCrossShardProcessing" component={SendCrossShardProcessingScreen} /> */}
    </Stack.Navigator>
  );
};
