import { createNativeStackNavigator } from '@react-navigation/native-stack';
import React from 'react';

import {
  WalletCreateScreen,
  WalletRestoreByMnemonicScreen,
  WalletRestoreByPrivateKeyScreen,
} from '../screens';

export type WalletStackParamList = {
  WalletCreate: undefined;
  WalletRestoreByMnemonic: undefined;
  WalletRestoreByPrivateKey: undefined;
};

const Stack = createNativeStackNavigator<WalletStackParamList>();

export const WalletStackNavigator = () => {
  return (
    <Stack.Navigator
      screenOptions={{
        headerShown: false,
      }}>
      <Stack.Screen name="WalletCreate" component={WalletCreateScreen} />
      <Stack.Screen
        name="WalletRestoreByMnemonic"
        component={WalletRestoreByMnemonicScreen}
      />
      <Stack.Screen
        name="WalletRestoreByPrivateKey"
        component={WalletRestoreByPrivateKeyScreen}
      />
    </Stack.Navigator>
  );
};
