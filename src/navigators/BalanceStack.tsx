import { NavigatorScreenParams } from '@react-navigation/native';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import React from 'react';

import { BalanceScreen } from '../screens';
import {
  ReceiveNavigatorParamList,
  ReceiveStackNavigator,
} from './ReceiveStack';
import { SendNavigatorParamList, SendStackNavigator } from './SendSctack';

export type BalanceNavigatorParamList = {
  Balance: undefined;
  // lockedBalance: LockedBalanceScreenParams
  SendStack: NavigatorScreenParams<SendNavigatorParamList>;
  ReceiveStack: NavigatorScreenParams<ReceiveNavigatorParamList>;
};

const Stack = createNativeStackNavigator<BalanceNavigatorParamList>();

export const BalanceStackNavigator = () => {
  return (
    <Stack.Navigator
      screenOptions={{
        headerShown: false,
      }}
    >
      <Stack.Screen name="Balance" component={BalanceScreen} />
      {/* <Stack.Screen name="lockedBalance" component={LockedBalanceScreen} /> */}
      <Stack.Screen name="SendStack" component={SendStackNavigator} />
      <Stack.Screen name="ReceiveStack" component={ReceiveStackNavigator} />
    </Stack.Navigator>
  );
};
