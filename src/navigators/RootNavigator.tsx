import {
  DefaultTheme,
  LinkingOptions,
  NavigationContainer,
  NavigationContainerRef,
  NavigatorScreenParams,
} from '@react-navigation/native';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import { StackScreenProps } from '@react-navigation/stack';
import { useColorModeValue, useToken } from 'native-base';
import React from 'react';
import { Linking } from 'react-native';
import { Host } from 'react-native-portalize';

import {
  OnboardingStackNavigator,
  OnboardingStackParamList,
} from './OnboardingStack';
import { TabsStackNavigator, TabsStackParamList } from './TabsStack';

export type RootStackParamList = {
  OnboardingStack: NavigatorScreenParams<OnboardingStackParamList>;
  TabsStack: NavigatorScreenParams<TabsStackParamList>;
};

export type RootStackScreenProps<T extends keyof RootStackParamList> =
  StackScreenProps<RootStackParamList, T>;

type RootNavigatorProps = {
  showOnboardingScreen: boolean;
} & Partial<React.ComponentProps<typeof NavigationContainer>>;

declare global {
  // eslint-disable-next-line @typescript-eslint/no-namespace
  namespace ReactNavigation {
    // eslint-disable-next-line @typescript-eslint/no-empty-interface
    interface RootParamList extends RootStackParamList {}
  }
}

const RootStack = createNativeStackNavigator<RootStackParamList>();

export const RootNavigator = React.forwardRef<
  NavigationContainerRef<RootStackParamList>,
  RootNavigatorProps
>(({ showOnboardingScreen, ...rest }, ref) => {
  const color = useToken('colors', useColorModeValue('offWhite', 'dark.500'));

  const linking: LinkingOptions<RootStackParamList> = {
    prefixes: ['peacewallet://'],
    getInitialURL: async () => {
      const url = await Linking.getInitialURL();

      if (url != null) {
        console.log('url 0', url);
        return url;
      }
    },
    // config: {
    //   screens: {
    //     OnboardingStack: {
    //       screens: {
    //         balance: {
    //           initialRouteName: 'balance',
    //           screens: {
    //             balance: 'balance',
    //             sendStack: {
    //               screens: {
    //                 sendCreate: 'send/:coinName/:address/:amount?/:shardID?',
    //               },
    //             },
    //           },
    //         },
    //       },
    //     },
    //   },
    // },
  };

  return (
    <NavigationContainer
      {...rest}
      ref={ref}
      theme={{
        dark: true,
        colors: {
          ...DefaultTheme.colors,
          background: color,
          card: color,
        },
      }}
      linking={linking}>
      <Host>
        <RootStack.Navigator
          screenOptions={{
            headerShown: false,
          }}
          initialRouteName={
            showOnboardingScreen ? 'OnboardingStack' : 'TabsStack'
          }>
          {showOnboardingScreen && (
            <RootStack.Screen
              name="OnboardingStack"
              component={OnboardingStackNavigator}
            />
          )}

          <RootStack.Screen name="TabsStack" component={TabsStackNavigator} />
        </RootStack.Navigator>
      </Host>
    </NavigationContainer>
  );
});

RootNavigator.displayName = 'RootNavigator';

const exitRoutes = ['onboarding', 'balance'];
export const canExit = (routeName: string) => exitRoutes.includes(routeName);
