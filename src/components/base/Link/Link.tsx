import React, { FC } from 'react';
import { Linking } from 'react-native';

import { Pressable } from '../Pressable';
import { Text } from '../Text';

export type LinkProps = {
  url: string;
  children?: React.ReactNode;
};

export const Link: FC<LinkProps> = (props) => {
  const { url, children } = props;

  const onOpen = async () => {
    const supported = await Linking.canOpenURL(url);

    if (supported) {
      await Linking.openURL(url);
      return;
    }

    try {
      await Linking.openURL(url);
    } catch {
      console.log('cant open url', url);
    }
  };

  return (
    <Pressable onPress={onOpen}>
      <Text numberOfLines={1} ellipsizeMode="middle" underline>
        {children}
      </Text>
    </Pressable>
  );
};
