import { Spacer as NBSpacer } from 'native-base';
import { FC } from 'react';

export type SpacerProps = {
  //
};

export const Spacer: FC<SpacerProps> = (props) => {
  return <NBSpacer {...props} />;
};
