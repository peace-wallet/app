import { theme } from '../../../theme';
import { Box, useColorMode, useTheme } from 'native-base';
import { StatusBar, StyleSheet, View, ViewProps } from 'react-native';
import {
  SafeAreaView,
  useSafeAreaInsets,
} from 'react-native-safe-area-context';

import { HeaderProps } from '../Header';
import { Text } from '../Text';

export type ScreenProps = ViewProps & {
  /**
   * Children components.
   */
  children?: React.ReactNode;

  /**
   * Header component.
   */
  header?: React.ReactElement<HeaderProps>;

  /**
   * Should we not wrap in SafeAreaView? Defaults to false.
   */
  unsafeTop?: boolean;

  /**
   * Should we not wrap in SafeAreaView? Defaults to false.
   */
  unsafeBottom?: boolean;

  /**
   * Layout level
   */
  level?: '1' | '2' | '3' | '4';

  warningText?: string;
};

export const Screen = (props: ScreenProps) => {
  const { header, children, warningText, ...rest } = props;
  const { colors } = useTheme();
  const { colorMode } = useColorMode();

  const insets = useSafeAreaInsets();

  const insetStyle = {
    paddingTop: props.unsafeTop ? 0 : insets.top > 0 ? 0 : theme.space[4],
    paddingBottom: props.unsafeBottom ? 0 : insets.bottom,
  };

  return (
    <View style={styles.screen} {...rest}>
      <SafeAreaView edges={['top']} />
      <StatusBar
        animated={true}
        barStyle={colorMode === 'light' ? 'dark-content' : 'light-content'}
        backgroundColor={
          colorMode === 'light' ? colors.offWhite : colors.dark[500]
        }
      />

      {warningText && (
        <Box
          style={[styles.badge, { top: insets.top - 4 }]}
          backgroundColor="red.500">
          <Text numberOfLines={1} style={styles.badgeText} color="white">
            {warningText}
          </Text>
        </Box>
      )}

      <Box flex={1} alignItems="stretch" style={insetStyle}>
        {header}
        {children}
      </Box>
    </View>
  );
};

const styles = StyleSheet.create({
  badge: {
    alignItems: 'center',
    flexDirection: 'row',
    justifyContent: 'center',
    left: 0,
    paddingHorizontal: theme.space[3],
    paddingVertical: 0,
    position: 'absolute',
    right: 0,
    zIndex: 9999,
  },
  badgeText: {
    fontSize: 10,
    lineHeight: 14,
  },
  flex: {
    flex: 1,
  },
  screen: {
    flex: 1,
    height: '100%',
  },
  screenFooter: {
    left: theme.space[2],
    position: 'absolute',
    right: theme.space[2],
  },
});
