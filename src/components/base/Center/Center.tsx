import { Center as NBCenter, ICenterProps } from 'native-base';
import { FC } from 'react';

export type CenterProps = ICenterProps;

export const Center: FC<CenterProps> = (props) => {
  return <NBCenter {...props} />;
};
