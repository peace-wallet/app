import { theme } from '../../../theme';
import { Box, HStack, Icon } from '../';
import { BottomTabBarProps } from '@react-navigation/bottom-tabs';
import { Route } from '@react-navigation/native';
import { useColorModeValue } from 'native-base';
import React, { FC, useEffect, useMemo, useRef } from 'react';
import { Animated, Platform, StyleSheet, TouchableOpacity } from 'react-native';
import { useSafeAreaInsets } from 'react-native-safe-area-context';

import { getActiveRouteName } from '../../../utils/navigation';

const isIOS = Platform.OS === 'ios';

type TabBarProps = BottomTabBarProps;

const BottomTabBar: FC<TabBarProps> = props => {
  const { state, descriptors, navigation } = props;

  const routeName = getActiveRouteName(state);

  const tabs = useMemo(() => ['BalanceStack', 'SettingsStack'], []);
  const screens = useMemo(() => [...tabs, 'Balance', 'Settings'], [tabs]);

  const insets = useSafeAreaInsets();
  const tabBarPositionY = useRef(new Animated.Value(0)).current;

  const activeColor = useColorModeValue('primary.500', 'white');
  const inactiveColor = useColorModeValue('primary.100', 'whiteAlpha.500');

  useEffect(() => {
    Animated.spring(tabBarPositionY, {
      toValue: screens?.indexOf(routeName) !== -1 ? 0 : 100,
      speed: 14,
      useNativeDriver: true,
    }).start();
  }, [routeName, screens, tabBarPositionY, tabs]);

  const onPress = (route: Route<string>, isFocused: boolean) => {
    const event = navigation.emit({
      type: 'tabPress',
      target: route.key,
      canPreventDefault: true,
    });

    if (!isFocused && !event.defaultPrevented) {
      navigation.navigate(route.name);
    }
  };

  const onLongPress = (route: Route<string>) => {
    navigation.emit({
      type: 'tabLongPress',
      target: route.key,
    });
  };

  const renderTabIcon = (
    isFocused: boolean,
    routeName: string,
  ): React.ReactNode => {
    let icon = null;

    switch (routeName) {
      case tabs[0]:
        icon = (
          <Icon.Wallet
            size={7}
            color={isFocused ? activeColor : inactiveColor}
          />
        );
        break;
      case tabs[1]:
        icon = (
          <Icon.Settings
            size={7}
            color={isFocused ? activeColor : inactiveColor}
          />
        );
        break;

      default:
        break;
    }

    return icon;
  };

  return (
    <Animated.View
      style={[
        styles.container,
        {
          bottom: insets.bottom || theme.space[2],
          transform: [
            {
              translateY: tabBarPositionY,
            },
          ],
        },
        Platform.OS === 'android'
          ? {
              elevation: 5,
              shadowColor: 'rgba(0,0,0,0.3)',
            }
          : theme.shadows[2],
      ]}>
      <Box
        style={styles.tabBar}
        bgColor={useColorModeValue('white', 'dark.500')}>
        {state.routes.map((route, index) => {
          const { options } = descriptors[route.key];
          const isFocused = state.index === index;

          return (
            <TouchableOpacity
              key={`tab-${index}`}
              style={styles.tab}
              activeOpacity={0.8}
              accessibilityRole="button"
              accessibilityState={isFocused ? { selected: true } : {}}
              accessibilityLabel={options.tabBarAccessibilityLabel}
              onPress={() => onPress(route, isFocused)}
              onLongPress={() => onLongPress(route)}>
              <HStack alignItems="center" justifyContent="center">
                {renderTabIcon(isFocused, route.name)}
              </HStack>
            </TouchableOpacity>
          );
        })}
      </Box>
    </Animated.View>
  );
};

const styles = StyleSheet.create({
  container: {
    borderRadius: 28,
    bottom: theme.space[16],
    height: 56,
    left: theme.space[16],
    overflow: isIOS ? 'visible' : 'hidden',
    position: 'absolute',
    right: theme.space[16],
  },
  tab: {
    alignContent: 'center',
    alignItems: 'center',
    flex: 1,
    height: '100%',
    justifyContent: 'center',
  },
  tabBar: {
    alignItems: 'center',
    borderRadius: 28,
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'space-around',
    overflow: 'hidden',
    paddingHorizontal: theme.space[6],
  },
});

export { BottomTabBar };
