import { Switch as NBSwitch, ISwitchProps } from 'native-base';
import React, { FC } from 'react';

export type SwitchProps = ISwitchProps;

export const Switch: FC<SwitchProps> = (props) => {
  return <NBSwitch {...props} />;
};
