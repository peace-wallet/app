import { VStack as NBVStack } from 'native-base';
import { IVStackProps } from 'native-base/src/components/primitives/Stack/VStack';
import { FC } from 'react';

export type VStackProps = IVStackProps;

export const VStack: FC<VStackProps> = (props) => {
  return <NBVStack {...props} />;
};

