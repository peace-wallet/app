import { useTheme } from 'native-base';
import { FC } from 'react';
import { StyleSheet } from 'react-native';
import { useSafeAreaInsets } from 'react-native-safe-area-context';
import RNToast, { BaseToastProps } from 'react-native-toast-message';

import {Box} from '../Box';
import {HStack} from '../HStack';
import {Icon} from '../Icon';
import {Text} from '../Text';
import {VStack} from '../VStack';

const ToastsContainer: FC = () => {
  const insets = useSafeAreaInsets();
  const { colors } = useTheme();

  const stylesByStatus = {
    success: {
      backgroundColor: colors.green[500],
      shadowColor: colors.green[500],
    },
    error: {
      backgroundColor: colors.red[500],
      shadowColor: colors.red[500],
    },
    info: {
      backgroundColor: colors.lightBlue[500],
      shadowColor: colors.lightBlue[500],
    },
  };

  const renderIcon = (status: 'success' | 'error' | 'info') => {
    if (status === 'success') return <Icon.Checkmark color="white" size={5} />;

    if (status === 'error') return <Icon.Info color="white" size={5} />;

    return <Icon.Info color="white" size={5} />;
  };

  const renderToast = (
    status: 'success' | 'error' | 'info',
    internalState: BaseToastProps
  ) => (
    <Box
      style={[styles.container, stylesByStatus[status], {}]}
      px="3"
      py="2"
      mx="2"
      my="1"
    >
      <HStack
        flex={1}
        space="sm"
        alignItems="center"
        justifyContent="flex-start"
      >
        {renderIcon(status)}
        <VStack flex={1} alignItems="flex-start" justifyContent="center">
          {internalState.text1 && (
            <Text color="white" fontSize="sm" bold>
              {internalState.text1}
            </Text>
          )}
          {internalState.text2 && (
            <Text color="whiteAlpha.900" fontSize="sm">
              {internalState.text2}
            </Text>
          )}
        </VStack>
      </HStack>
    </Box>
  );

  const config = {
    info: (internalState: BaseToastProps) => renderToast('info', internalState),
    success: (internalState: BaseToastProps) =>
      renderToast('success', internalState),
    error: (internalState: BaseToastProps) =>
      renderToast('error', internalState),
  };

  return (
    <RNToast
      config={config}
      topOffset={insets.top}
      // ref={(ref) => RNToast.setRef(ref)}
    />
  );
};

const styles = StyleSheet.create({
  container: {
    borderRadius: 5,
    elevation: 2,
    flexDirection: 'row',
    shadowOpacity: 0.05,
    shadowRadius: 100,
  },
});

export { ToastsContainer, RNToast as Toast };
