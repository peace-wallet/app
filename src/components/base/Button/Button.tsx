import { Button as NBButton, IButtonProps } from 'native-base';
import { IButtonGroupProps } from 'native-base/lib/typescript/components/primitives/Button';
import ButtonGroup from 'native-base/src/components/primitives/Button/ButtonGroup';
import React, { FC } from 'react';
import { View } from 'react-native';

export type ButtonProps = IButtonProps;

export type ButtonComponentType = ((
  props: IButtonProps & { ref?: unknown }
) => React.JSX.Element) & {
  Group: React.MemoExoticComponent<
    (props: IButtonGroupProps & { ref?: unknown }) => React.JSX.Element
  >;
};

const ButtonMain: FC<ButtonProps> = (props) => {
  const {
    children,
    size = 'lg',
    startIcon,
    endIcon,
    flex,
    paddingX,
    onPress,
    ...rest
  } = props;

  return (
    <NBButton
      size={size}
      flex={flex}
      startIcon={
        startIcon && flex ? (
          <View style={{ flex: 1, alignItems: 'flex-start' }}>{startIcon}</View>
        ) : (
          startIcon
        )
      }
      endIcon={
        endIcon && flex ? (
          <View style={{ flex: 1, alignItems: 'flex-end' }}>{endIcon}</View>
        ) : (
          endIcon
        )
      }
      px={(endIcon || startIcon) && flex ? '2' : paddingX}
      onPress={onPress}
      {...rest}
    >
      {children}
    </NBButton>
  );
};

const ButtonTemp: any = ButtonMain;
ButtonTemp.Group = ButtonGroup;

export const Button = ButtonTemp as ButtonComponentType;
