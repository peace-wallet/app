import { Box, IBoxProps, useColorModeValue, useToken } from 'native-base';
import { FC } from 'react';
import { Platform, StyleSheet } from 'react-native';

export type CardProps = IBoxProps & {
  error?: boolean;
  bordered?: boolean;
};

export const Card: FC<CardProps> = (props) => {
  const {
    rounded = 'xl',
    p = 4,
    error = false,
    children,
    bordered,
    ...rest
  } = props;

  const borderColorToken = useColorModeValue('white', 'darkGray.500');
  const borderColor = useToken(
    'colors',
    useColorModeValue('primaryAlpha.50', 'whiteAlpha.100')
  );

  return (
    <Box
      rounded={rounded}
      borderWidth={bordered ? 1 : 0}
      borderColor={bordered ? borderColor : undefined}
      shadow={bordered ? undefined : 1}
      style={
        Platform.OS === 'android' && {
          elevation: 5,
          shadowColor: 'rgba(0,0,0,0.3)',
        }
      }
      p={p}
      _light={{ backgroundColor: 'white' }}
      _dark={{ backgroundColor: 'darkGray.500' }}
      {...rest}
    >
      {children}
      {error && (
        <Box
          style={styles.borderOverlay}
          rounded={rounded}
          borderWidth={1}
          pointerEvents="none"
          borderColor={error ? 'red.500' : borderColorToken}
        />
      )}
    </Box>
  );
};

const styles = StyleSheet.create({
  borderOverlay: {
    bottom: 0,
    left: 0,
    position: 'absolute',
    right: 0,
    top: 0,
  },
});
