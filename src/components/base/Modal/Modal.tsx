import { theme } from '../../../theme';
import { useColorModeValue, useToken } from 'native-base';
import React, { useEffect, useRef, useState } from 'react';
import { Keyboard, Platform, StyleSheet } from 'react-native';
import { Modalize, ModalizeProps } from 'react-native-modalize';
import { Portal } from 'react-native-portalize';
import { useSafeAreaInsets } from 'react-native-safe-area-context';

import { Box } from '../Box';

export type ModalProps = ModalizeProps & {
  isVisible: boolean;
};

export const Modal = ({
  isVisible,
  adjustToContentHeight = true,
  children,
  HeaderComponent,
  FooterComponent,
  ...rest
}: ModalProps) => {
  const bottomSheetModalRef = useRef<Modalize>(null);
  const insets = useSafeAreaInsets();

  const overlayBackground = useToken('colors', 'darkGrayAlpha.500');
  const modalBackground = useToken(
    'colors',
    useColorModeValue('white', 'darkGray.500'),
  );

  const [keyboardIsOpen, setKeyboardIsOpen] = useState(false);

  const keyboardWillShow = () => setKeyboardIsOpen(true);
  const keyboardWillHide = () => setKeyboardIsOpen(false);

  useEffect(() => {
    isVisible
      ? bottomSheetModalRef?.current?.open()
      : bottomSheetModalRef?.current?.close();
  }, [isVisible]);

  useEffect(() => {
    const keyboardWillShowSubscription = Keyboard.addListener(
      'keyboardWillShow',
      keyboardWillShow,
    );
    const keyboardWillHideSubscription = Keyboard.addListener(
      'keyboardWillHide',
      keyboardWillHide,
    );

    return () => {
      keyboardWillShowSubscription.remove();
      keyboardWillHideSubscription.remove();
    };
  }, []);

  return (
    <Portal>
      <Modalize
        ref={bottomSheetModalRef}
        adjustToContentHeight={adjustToContentHeight}
        modalStyle={[styles.root, { backgroundColor: modalBackground }]}
        childrenStyle={{
          paddingBottom:
            insets.bottom > 0 && !FooterComponent
              ? keyboardIsOpen
                ? theme.space[2]
                : insets.bottom
              : theme.space[0],
        }}
        overlayStyle={{
          backgroundColor: overlayBackground,
        }}
        handleStyle={{
          backgroundColor: useToken(
            'colors',
            useColorModeValue('primaryAlpha.200', 'whiteAlpha.200'),
          ),
        }}
        handlePosition="inside"
        HeaderComponent={
          <Box px="6" pt={HeaderComponent ? '10' : '6'} pb="2">
            {HeaderComponent}
          </Box>
        }
        FooterComponent={
          FooterComponent && (
            <Box
              p="2"
              paddingBottom={
                insets.bottom > 0
                  ? keyboardIsOpen
                    ? theme.space[2]
                    : insets.bottom
                  : Platform.OS === 'ios'
                  ? theme.space[2]
                  : 0
              }>
              {FooterComponent}
            </Box>
          )
        }
        {...rest}>
        {children && (
          <Box px="6" pb="6" pt="4">
            {children}
          </Box>
        )}
      </Modalize>
    </Portal>
  );
};

const styles = StyleSheet.create({
  root: {
    borderTopLeftRadius: theme.radii.md,
    borderTopRightRadius: theme.radii.md,
  },
});
