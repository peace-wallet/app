import { ITextProps, Text as NBText } from 'native-base';
import { FC } from 'react';
import { TextStyle } from 'react-native';

export type TextProps = ITextProps & {
  align?: 'left' | 'center' | 'right';
  secondary?: boolean;
};

export const Text: FC<TextProps> = (props) => {
  const {
    align = 'left',
    secondary = false,
    fontSize,
    fontWeight,
    lineHeight,
    color,
    children,
    ...rest
  } = props;

  const styles: TextStyle[] = [
    {
      textAlign: align,
    },
  ];

  const getFontSize = () => {
    if (secondary) {
      return 'sm';
    }

    return fontSize;
  };

  const getFontWeight = () => {
    if (secondary && !fontWeight) {
      return 500;
    }

    return fontWeight;
  };

  const getLineHeight = () => {
    if (secondary && !lineHeight) {
      return '24px';
    }

    return lineHeight;
  };

  const getLightTheme = () => {
    if (secondary && !color) {
      return { color: 'primaryAlpha.500' };
    }

    return { color: color ?? 'primary.500' };
  };

  const getDarkTheme = () => {
    if (secondary && !color) {
      return { color: 'whiteAlpha.500' };
    }

    return { color: color ?? 'white' };
  };

  return (
    <NBText
      fontSize={getFontSize()}
      lineHeight={getLineHeight()}
      fontWeight={getFontWeight()}
      _light={getLightTheme()}
      _dark={getDarkTheme()}
      style={styles}
      {...rest}
    >
      {children}
    </NBText>
  );
};
