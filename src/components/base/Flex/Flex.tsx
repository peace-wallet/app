import { Flex as NBFlex, IFlexProps } from 'native-base';
import { FC } from 'react';

export type FlexProps = IFlexProps;

export const Flex: FC<FlexProps> = (props) => {
  return <NBFlex {...props} />;
};
