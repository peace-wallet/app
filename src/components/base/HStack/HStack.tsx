import { HStack as NBHStack } from 'native-base';
import { IHStackProps } from 'native-base/src/components/primitives/Stack/HStack';
import { FC } from 'react';

export type HStackProps = IHStackProps;

export const HStack: FC<HStackProps> = (props) => {
  return <NBHStack {...props} />;
};
