import { Divider as NBDivider, IDividerProps } from 'native-base';
import { FC } from 'react';

export type DividerProps = IDividerProps;

export const Divider: FC<DividerProps> = (props) => {
  return <NBDivider {...props} />;
};
