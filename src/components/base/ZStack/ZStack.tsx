import { IZStackProps, ZStack as NBZStack } from 'native-base';
import React, { FC } from 'react';

export type ZStackProps = IZStackProps;

export const ZStack: FC<ZStackProps> = props => {
  return <NBZStack {...props} />;
};
