import { Spinner as NBSpinner, useColorModeValue } from 'native-base';
import { ISpinnerProps } from 'native-base';
import React, { FC } from 'react';

export type SpinnerProps = ISpinnerProps;

export const Spinner: FC<SpinnerProps> = (props) => {
  const { size = 22, ...rest } = props;

  return (
    <NBSpinner
      size={size}
      color={useColorModeValue('primaryAlpha.500', 'whiteAlpha.500')}
      {...rest}
    />
  );
};
