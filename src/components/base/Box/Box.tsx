import { Box as NBBox } from 'native-base';
import { IBoxProps } from 'native-base';
import { FC } from 'react';

export type BoxProps = IBoxProps;
export const Box: FC<BoxProps> = (props) => {
  return <NBBox {...props} />;
};
