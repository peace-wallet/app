import { theme } from '../../../theme';
import { Box, useColorModeValue } from 'native-base';
import { useState } from 'react';
import { LayoutChangeEvent, Platform, StyleSheet } from 'react-native';
import {
  KeyboardAwareFlatList,
  KeyboardAwareFlatListProps,
  KeyboardAwareScrollView,
  KeyboardAwareScrollViewProps,
  KeyboardAwareSectionList,
  KeyboardAwareSectionListProps,
} from 'react-native-keyboard-aware-scroll-view';
import { useSafeAreaInsets } from 'react-native-safe-area-context';

export type ScreenContentProps = KeyboardAwareScrollViewProps & {
  /**
   * Children components.
   */
  children?: React.ReactNode;

  /**
   * Should we have paddings? Defaults to false.
   */
  paddingLess?: boolean;
};

export type ScreenScrollViewContentProps = KeyboardAwareScrollViewProps & {
  /**
   * Children components.
   */
  children?: React.ReactNode;

  /**
   * Footer component.
   */
  footer?: React.ReactNode;

  /**
   * Should we have paddings? Defaults to false.
   */
  paddingLess?: boolean;
};

export type ScreenFlatListContentProps = KeyboardAwareFlatListProps<any> & {
  /**
   * Should we have paddings? Defaults to false.
   */
  paddingLess?: boolean;
};

export type ScreenSectionListContentProps =
  KeyboardAwareSectionListProps<any> & {
    /**
     * Should we have paddings? Defaults to false.
     */
    paddingLess?: boolean;
  };

export const ScreenContent = (props: ScreenContentProps) => {
  const {
    children,
    style,
    paddingLess,
    extraScrollHeight = 64,
    scrollEnabled = false,
    showsVerticalScrollIndicator = false,
    ...rest
  } = props;
  const insets = useSafeAreaInsets();

  const paddingStyle = {
    padding: paddingLess ? 0 : theme.space[2],
    paddingBottom: paddingLess ? 0 : insets.bottom > 0 ? 0 : theme.space[2],
  };

  return (
    <KeyboardAwareScrollView
      contentContainerStyle={[
        !scrollEnabled && styles.flex,
        paddingStyle,
        style,
      ]}
      extraScrollHeight={extraScrollHeight}
      scrollEnabled={scrollEnabled}
      showsVerticalScrollIndicator={showsVerticalScrollIndicator}
      {...rest}>
      {children}
    </KeyboardAwareScrollView>
  );
};

export const ScreenScrollViewContent = (
  props: ScreenScrollViewContentProps,
) => {
  const {
    children,
    footer,
    style,
    paddingLess,
    extraScrollHeight,
    scrollEnabled = true,
    showsVerticalScrollIndicator = false,
    ...rest
  } = props;

  const insets = useSafeAreaInsets();
  const bgColorToken = useColorModeValue('offWhite', 'dark.500');

  const defaultExtraScrollHeight =
    Platform.OS === 'android' ? (footer ? 88 : 24) : 64;

  const [footerHeight, setFooterHeight] = useState(0);

  const paddingStyle = {
    padding: paddingLess ? 0 : theme.space[2],
    paddingBottom:
      (paddingLess ? 0 : insets.bottom > 0 ? 0 : theme.space[2]) +
      (footerHeight ? footerHeight + theme.space[2] : 0),
  };

  const bottomInsetsStyle = {
    bottom: insets.bottom > 0 ? insets.bottom : theme.space[2],
  };

  const handleFooterLayout = ({ nativeEvent }: LayoutChangeEvent) =>
    setFooterHeight(nativeEvent.layout.height);

  return (
    <>
      <KeyboardAwareScrollView
        enableOnAndroid={true}
        contentContainerStyle={[
          !scrollEnabled && styles.flex,
          paddingStyle,
          style,
        ]}
        extraScrollHeight={extraScrollHeight ?? defaultExtraScrollHeight}
        scrollEnabled={scrollEnabled}
        showsVerticalScrollIndicator={showsVerticalScrollIndicator}
        {...rest}>
        {children}
      </KeyboardAwareScrollView>
      {footer && (
        <Box
          style={[styles.screenFooter, bottomInsetsStyle]}
          shadow="1"
          bgColor={bgColorToken}
          rounded="lg"
          onLayout={handleFooterLayout}>
          {footer}
        </Box>
      )}
    </>
  );
};

export const ScreenFlatListContent = (props: ScreenFlatListContentProps) => {
  const {
    style,
    paddingLess,
    scrollEnabled = true,
    showsVerticalScrollIndicator = false,
    ...rest
  } = props;

  const insets = useSafeAreaInsets();

  const paddingStyle = {
    padding: paddingLess ? 0 : theme.space[2],
    paddingBottom: paddingLess ? 0 : insets.bottom > 0 ? 0 : theme.space[2],
  };

  return (
    <KeyboardAwareFlatList
      contentContainerStyle={[
        !scrollEnabled && styles.flex,
        paddingStyle,
        style,
      ]}
      extraScrollHeight={64}
      showsVerticalScrollIndicator={showsVerticalScrollIndicator}
      {...rest}
    />
  );
};

export const ScreenSectionListContent = (
  props: ScreenSectionListContentProps,
) => {
  const {
    style,
    paddingLess,
    scrollEnabled = true,
    showsVerticalScrollIndicator = false,
    ...rest
  } = props;

  const insets = useSafeAreaInsets();

  const paddingStyle = {
    padding: paddingLess ? 0 : theme.space[2],
    paddingBottom: paddingLess ? 0 : insets.bottom > 0 ? 0 : theme.space[2],
  };

  return (
    <KeyboardAwareSectionList
      contentContainerStyle={[
        !scrollEnabled && styles.flex,
        paddingStyle,
        style,
      ]}
      extraScrollHeight={64}
      showsVerticalScrollIndicator={showsVerticalScrollIndicator}
      {...rest}
    />
  );
};

const styles = StyleSheet.create({
  badge: {
    alignItems: 'center',
    flexDirection: 'row',
    justifyContent: 'center',
    left: 0,
    paddingHorizontal: theme.space[3],
    paddingVertical: 0,
    position: 'absolute',
    right: 0,
    zIndex: 9999,
  },
  badgeText: {
    fontSize: 10,
    lineHeight: 14,
  },
  flex: {
    flex: 1,
  },
  screen: {
    flex: 1,
    height: '100%',
  },
  screenFooter: {
    left: theme.space[2],
    position: 'absolute',
    right: theme.space[2],
  },
});
