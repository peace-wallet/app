import { useColorModeValue, useToken } from 'native-base';
import React, { FC } from 'react';
import { RefreshControl as NRefreshControl, RefreshControlProps as NRefreshControlProps } from 'react-native';

export type RefreshControlProps = NRefreshControlProps;

export const RefreshControl: FC<RefreshControlProps> = (props) => {
  const titleColor = useToken(
    'colors',
    useColorModeValue('primary.500', 'white')
  );
  const tintColor = useToken(
    'colors',
    useColorModeValue('primaryAlpha.500', 'whiteAlpha.500')
  );
  const colors = useToken(
    'colors',
    useColorModeValue('primaryAlpha.500', 'primaryAlpha.500')
  );

  return (
    <NRefreshControl
      titleColor={titleColor}
      colors={[colors]}
      tintColor={tintColor}
      {...props}
    />
  );
};
