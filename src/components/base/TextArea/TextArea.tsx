import { ITextAreaProps, TextArea as NBTextArea } from 'native-base';
import { FC } from 'react';

export type TextAreaProps = ITextAreaProps & {
  error?: boolean;
};

export const TextArea: FC<TextAreaProps> = (props) => {
  const {
    placeholder,
    color,
    placeholderTextColor,
    error = false,
    ...rest
  } = props;

  return (
    <NBTextArea
      color={error ? 'red.500' : color}
      placeholder={placeholder}
      autoCompleteType="off"
      placeholderTextColor={error ? 'red.400' : placeholderTextColor}
      {...rest}
    />
  );
};
