import { Pressable as NBPressable } from 'native-base';
import { IPressableProps } from 'native-base';
import { FC, forwardRef } from 'react';

export type PressableProps = IPressableProps;

export const Pressable: FC<PressableProps> = forwardRef((props, ref) => {
  const { _pressed = { opacity: 0.8 }, ...rest } = props;

  return <NBPressable _pressed={_pressed} ref={ref} {...rest} />;
});
