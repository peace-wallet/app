import { sizing } from '../../../theme';
import { useNavigation } from '@react-navigation/native';
import React, { FC } from 'react';
import { StyleSheet } from 'react-native';

import {Box} from '../Box';
import {Flex} from '../Flex';
import {HStack} from '../HStack';
import {Icon} from '../Icon';
import { IconButton } from '../IconButton';
import {Text} from '../Text';

export type HeaderProps = {
  children?: string;
  hasBackAction?: boolean;
  rightAction?: React.ReactElement;
};

export const Header: FC<HeaderProps> = ({
  children,
  hasBackAction = true,
  rightAction,
}) => {
  const navigation = useNavigation();

  const onBackPress = () => navigation.goBack();

  return (
    <Box style={styles.root}>
      <HStack flex={1} justifyContent="space-between" alignItems="center">
        {hasBackAction && (
          <IconButton
            variant="unstyled"
            icon={<Icon.ChevronLeft size={6} />}
            _pressed={{ opacity: 0.8 }}
            onPress={onBackPress}
          />
        )}
        <Flex flex={1} pl={!hasBackAction ? 6 : '0'}>
          <Text
            _light={{ color: 'primary.600' }}
            _dark={{ color: 'white' }}
            fontSize={16}
            textTransform="uppercase">
            {children}
          </Text>
        </Flex>
        {rightAction && <Box pr={2}>{rightAction}</Box>}
      </HStack>
    </Box>
  );
};

const styles = StyleSheet.create({
  root: {
    height: sizing.headerHeight,
  },
});

