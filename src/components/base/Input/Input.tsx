import { Input as NBInput, IInputProps } from 'native-base';
import { FC } from 'react';

export type InputProps = IInputProps & {
  error?: boolean;
};

export const Input: FC<InputProps> = (props) => {
  const { color, placeholderTextColor, error, ...rest } = props;

  return (
    <NBInput
      color={error ? 'red.500' : color}
      placeholderTextColor={error ? 'red.400' : placeholderTextColor}
      {...rest}
    />
  );
};
