import { IconButton as NBIconButton, IIconButtonProps } from 'native-base';
import { FC } from 'react';

export type IconButtonProps = IIconButtonProps;

export const IconButton: FC<IconButtonProps> = (props) => {
  const { _pressed = { opacity: 0.8 }, ...rest } = props;
  return <NBIconButton _pressed={_pressed} {...rest} />;
};
