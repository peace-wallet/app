import { Select as NBSelect, ISelectItemProps, ISelectProps } from 'native-base';
import React, { FC, MutableRefObject } from 'react';

export type SelectProps = ISelectProps;

export type SelectComponentType = ((
  props: ISelectProps & SelectProps & { ref?: unknown }
) => JSX.Element) & {
  Item: React.MemoExoticComponent<
    (
      props: ISelectItemProps & {
        ref?: MutableRefObject<unknown>;
      }
    ) => JSX.Element
  >;
};

interface CompoundedComponent {
  Item: typeof NBSelect.Item;
}

export const Select: FC<SelectProps> & CompoundedComponent = (props) => {
  const { ...rest } = props;

  return <NBSelect {...rest} />;
};

Select.Item = NBSelect.Item;
