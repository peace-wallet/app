import {
  Flex,
  HStack,
  Icon,
  IconButton,
  Link,
  Pressable,
  Text,
  Toast,
  VStack,
} from '../base';
import Clipboard from '@react-native-clipboard/clipboard';
import { Popover } from 'native-base';
import numeral from 'numeral';
import React, { FC } from 'react';
import { useTranslation } from 'react-i18next';

import { Coin, Network } from '../../models';
import { CoinsFormatter } from '../../utils/formatters';

export type TransactionDetailsProps = {
  network: Network;
  coin: Coin;
  address: string;
  sourceShardId: number;
  destinationShardId?: number;
  sourceAmount: number;
  destinationAmount?: number;
  transactionFee?: number;
  hash?: string;
  confirmations?: number;
  blockNumber?: number;
  exchangeAgentFee?: number;
  exchangeAgentUrl?: string;
  direction?: 'out' | 'in' | string;
  lockTransactionHash?: string;
};

const TransactionDetails: FC<TransactionDetailsProps> = props => {
  const {
    coin,
    network,
    sourceAmount,
    destinationAmount,
    address,
    sourceShardId,
    destinationShardId,
    transactionFee,
    hash,
    confirmations,
    blockNumber,
    exchangeAgentFee,
    exchangeAgentUrl,
    direction = 'out',
    lockTransactionHash,
  } = props;
  const { t } = useTranslation();

  const _sourceAmount = CoinsFormatter.toBiggestUnits(coin.name, sourceAmount);
  const formattedSourceAmount =
    _sourceAmount > 10000
      ? numeral(_sourceAmount).format('0.00a')
      : _sourceAmount;

  const _destinationAmount = CoinsFormatter.toBiggestUnits(
    coin.name,
    destinationAmount,
  );
  const formattedDestinationAmount =
    _destinationAmount > 10000
      ? numeral(_destinationAmount).format('0.00a')
      : _destinationAmount;

  const addressUrl = `https://explore.jax.net/shards/addresses/${address}?network=${network}`;
  const transactionUrl =
    coin.name === 'btc'
      ? `https://www.blockchain.com/en/search?search=${hash}`
      : `https://explore.jax.net/shards/${sourceShardId}/transactions/${hash}?network=${network}`;
  const lockTransactionUrl = `https://explore.jax.net/shards/${
    direction === 'out' ? sourceShardId : destinationShardId
  }/transactions/${lockTransactionHash}?network=${network}`;

  const isCrossShardTransaction =
    destinationShardId !== null &&
    destinationShardId !== undefined &&
    sourceShardId !== destinationShardId;

  const onCopy = (text: string) => {
    Clipboard.setString(text);
    Toast.show({
      type: 'info',
      text1: t('notifications.copied'),
      text2: text,
    });
  };

  const renderLabel = (text: string) => (
    <Text
      fontSize="sm"
      _light={{ color: 'primaryAlpha.500' }}
      _dark={{ color: 'whiteAlpha.500' }}>
      {text}
    </Text>
  );

  const renderRegularTransactionDetails = () => (
    <VStack space="lg">
      <HStack>
        <VStack flex={1}>
          <HStack space={1}>
            {_sourceAmount > 10000 ? (
              <Text>{`${formattedSourceAmount}`}</Text>
            ) : (
              <Text>{`${_sourceAmount}`}</Text>
            )}
            <Text color={`coins.${coin.name}.500`}>{coin.title}</Text>
          </HStack>
          {renderLabel(t('labels.amount'))}
        </VStack>
        {coin.name === 'jax' && (
          <VStack flex={1}>
            <Text>{`${t('labels.shard')} ${sourceShardId}`}</Text>
            {renderLabel(t('labels.shard'))}
          </VStack>
        )}
      </HStack>

      {transactionFee && (
        <HStack>
          <VStack>
            <HStack space={1}>
              <Text>{`${CoinsFormatter.toBiggestUnits(
                coin.name,
                transactionFee,
              )}`}</Text>
              <Text color={`coins.${coin.name}.500`}>{coin.title}</Text>
            </HStack>

            {renderLabel(t('labels.fee'))}
          </VStack>
        </HStack>
      )}
    </VStack>
  );

  const renderCrossShardTransactionDetails = () => (
    <VStack space="lg">
      <HStack>
        <VStack flex={1}>
          <HStack space={1}>
            {_sourceAmount > 10000 ? (
              <Popover
                trigger={triggerProps => {
                  return (
                    <Pressable {...triggerProps}>
                      <Text>{`${formattedSourceAmount}`}</Text>
                    </Pressable>
                  );
                }}>
                <Popover.Content zIndex={9999}>
                  <Popover.Arrow />
                  <Popover.Body>
                    <Text fontSize={14}>{`${sourceAmount}`}</Text>
                  </Popover.Body>
                </Popover.Content>
              </Popover>
            ) : (
              <Text>{`${_sourceAmount}`}</Text>
            )}
            <Text color={`coins.${coin.name}.500`}>{coin.title}</Text>
          </HStack>
          {renderLabel(t('labels.willBeSent'))}
        </VStack>
        <VStack flex={1}>
          <Text>{`${t('labels.shard')} ${sourceShardId}`}</Text>
          {renderLabel(t('labels.sourceShard'))}
        </VStack>
      </HStack>

      <HStack>
        <VStack flex={1}>
          <HStack space={1}>
            {_destinationAmount > 10000 ? (
              <Popover
                trigger={triggerProps => {
                  return (
                    <Pressable {...triggerProps}>
                      <Text>{`${formattedDestinationAmount}`}</Text>
                    </Pressable>
                  );
                }}>
                <Popover.Content zIndex={9999}>
                  <Popover.Arrow />
                  <Popover.Body>
                    <Text fontSize={14}>{`${destinationAmount}`}</Text>
                  </Popover.Body>
                </Popover.Content>
              </Popover>
            ) : (
              <Text>{`${_destinationAmount}`}</Text>
            )}
            <Text color={`coins.${coin.name}.500`}>{coin.title}</Text>
          </HStack>

          {renderLabel(t('labels.willBeReceived'))}
        </VStack>
        <VStack flex={1}>
          <Text>{`${t('labels.shard')} ${destinationShardId}`}</Text>
          {renderLabel(t('labels.destinationShard'))}
        </VStack>
      </HStack>

      <HStack>
        <VStack flex={1}>
          <HStack alignItems="center" space={2}>
            <HStack space={1}>
              <Text>{`${CoinsFormatter.toBiggestUnits(
                coin.name,
                transactionFee + exchangeAgentFee,
              )}`}</Text>
              <Text color={`coins.${coin.name}.500`}>{coin.title}</Text>
            </HStack>
            <Popover
              trigger={triggerProps => {
                return (
                  <Pressable {...triggerProps} alignSelf="center">
                    <Icon.Info size={18} />
                  </Pressable>
                );
              }}>
              <Popover.Content>
                <Popover.Arrow />
                <Popover.Body>
                  <VStack>
                    <Text fontSize={14}>{`${CoinsFormatter.toBiggestUnits(
                      coin.name,
                      transactionFee,
                    )} ${coin.title} — ${t('labels.networkFee')}`}</Text>
                    <Text fontSize={14}>{`${CoinsFormatter.toBiggestUnits(
                      coin.name,
                      exchangeAgentFee,
                    )} ${coin.title} — ${t('labels.exchangeAgentFee')}`}</Text>
                  </VStack>
                </Popover.Body>
              </Popover.Content>
            </Popover>
          </HStack>
          {renderLabel(t('labels.fee'))}
        </VStack>
      </HStack>
    </VStack>
  );

  return (
    <VStack space="lg">
      {address && (
        <VStack>
          <HStack alignItems="center">
            <Flex flex={1}>
              <Link url={addressUrl}>{address}</Link>
            </Flex>
            <IconButton
              variant="unstyled"
              icon={<Icon.Copy size="sm" />}
              size="sm"
              onPress={() => onCopy(address)}
            />
          </HStack>
          {renderLabel(
            t(`labels.${direction === 'in' ? 'sender' : 'receiver'}`),
          )}
        </VStack>
      )}

      {isCrossShardTransaction
        ? renderCrossShardTransactionDetails()
        : renderRegularTransactionDetails()}

      {exchangeAgentUrl && (
        <VStack>
          <HStack alignItems="center">
            <Flex flex={1}>
              <Text>{exchangeAgentUrl}</Text>
            </Flex>
            <IconButton
              variant="unstyled"
              size="sm"
              icon={<Icon.Copy size="sm" />}
              onPress={() => onCopy(exchangeAgentUrl)}
            />
          </HStack>
          {renderLabel(t('labels.exchangeAgent'))}
        </VStack>
      )}

      {confirmations !== null && confirmations !== undefined && (
        <HStack>
          <VStack flex={1}>
            <Text>{`${confirmations}`}</Text>
            {renderLabel(t('labels.confirmations'))}
          </VStack>
          {/* <VStack flex={1}>
            <Text>{`${blockNumber ?? 'Mempool'}`}</Text>
            {renderLabel(t('labels.blockNumber'))}
          </VStack> */}
        </HStack>
      )}

      {hash && (
        <VStack>
          <HStack alignItems="center">
            <Flex flex={1}>
              <Link url={transactionUrl}>{hash}</Link>
            </Flex>
            <IconButton
              variant="unstyled"
              icon={<Icon.Copy size="sm" />}
              size="sm"
              onPress={() => onCopy(transactionUrl)}
            />
          </HStack>
          {renderLabel(t('labels.hash'))}
        </VStack>
      )}

      {lockTransactionHash && (
        <VStack>
          <HStack alignItems="center">
            <Flex flex={1}>
              <Link url={lockTransactionUrl}>{lockTransactionHash}</Link>
            </Flex>
            <IconButton
              variant="unstyled"
              icon={<Icon.Copy size="sm" />}
              size="sm"
              onPress={() => onCopy(lockTransactionUrl)}
            />
          </HStack>
          {renderLabel(t('labels.lockTransactionHash'))}
        </VStack>
      )}
    </VStack>
  );
};

export { TransactionDetails };
