import { Box, Button, Icon, Spacer, Text, VStack } from '../base';
import { HStack } from 'native-base';
import React, { FC, useCallback, useEffect, useMemo, useState } from 'react';
import { useTranslation } from 'react-i18next';
import { Dimensions, SafeAreaView } from 'react-native';
import ReactNativeBiometrics, { BiometryTypes } from 'react-native-biometrics';

import { useAppStore } from '../../stores';

type PinCodeLockProps = {
  visible: boolean;
};

const PinCodeLock: FC<PinCodeLockProps> = ({ visible }: PinCodeLockProps) => {
  const { t } = useTranslation();

  const PIN_LENGTH = 4;

  const savedPinCode = useAppStore(state => state.pinCode);
  const savePinCode = useAppStore(state => state.savePinCode);
  const hidePinCodeScreen = useAppStore(state => state.hidePinCodeScreen);

  const [state, setState] = useState(savedPinCode ? 'enter' : 'create');
  const [open, setOpen] = useState(visible);

  const [biometricType, setBiometricType] = useState<
    'touchId' | 'faceId' | null
  >(null);

  const [pin, setPin] = useState<string[]>([]);
  const [pinBackup, setPinBackup] = useState<string[]>([]);

  const rnBiometrics = useMemo(() => new ReactNativeBiometrics(), []);

  const unlockByBiometrics = useCallback(() => {
    rnBiometrics
      .simplePrompt({ promptMessage: 'Confirm fingerprint' })
      .then(resultObject => {
        const { success } = resultObject;

        if (success) {
          hidePinCodeScreen();
        } else {
          console.log('user cancelled biometric prompt');
        }
      })
      .catch(() => {
        console.log('biometrics failed');
      });
  }, [hidePinCodeScreen, rnBiometrics]);

  const renderNumberButton = (text: string) => (
    <Button
      flex={1}
      variant="subtle"
      _light={{ bgColor: 'white' }}
      onPress={() => {
        if (pin.length < PIN_LENGTH) {
          setPin([...pin, text]);
        }
      }}>
      {text}
    </Button>
  );

  useEffect(() => {
    if (state === 'create' && pin.length === PIN_LENGTH) {
      setPinBackup(pin);
      setPin([]);
      setState('confirm');
    }

    if (state === 'confirm' && pin.length === PIN_LENGTH) {
      if (pinBackup.join('') === pin.join('')) {
        savePinCode(pin.join(''));
        hidePinCodeScreen();
      } else {
        setPin([]);
        setState('create');
      }
    }

    if (state === 'enter' && pin.length === PIN_LENGTH) {
      if (pin.join('') === savedPinCode) {
        hidePinCodeScreen();
      } else {
        setPin([]);
        setPinBackup([]);
      }
    }
  }, [hidePinCodeScreen, pin, pinBackup, savePinCode, savedPinCode, state]);

  useEffect(() => {
    setOpen(visible);
  }, [visible]);

  useEffect(() => {
    if (savedPinCode) {
      setState('enter');
    } else {
      setState('create');
    }
  }, [savedPinCode]);

  useEffect(() => {
    rnBiometrics.isSensorAvailable().then(resultObject => {
      const { available, biometryType } = resultObject;

      if (available) {
        unlockByBiometrics();

        if (biometryType === BiometryTypes.TouchID) {
          setBiometricType('touchId');
        } else if (biometryType === BiometryTypes.FaceID) {
          setBiometricType('faceId');
        } else if (biometryType === BiometryTypes.Biometrics) {
          setBiometricType('touchId');
        }
      } else {
        setBiometricType(null);
      }
    });
  }, [rnBiometrics, unlockByBiometrics]);

  return open ? (
    <VStack
      position="absolute"
      top={0}
      bottom={0}
      left={0}
      right={0}
      alignItems="center"
      justifyContent="center"
      _light={{ bg: 'offWhite' }}
      _dark={{ bg: 'dark.500' }}>
      <SafeAreaView>
        <VStack width={Dimensions.get('screen').width} flex={1} px={2}>
          <HStack justifyContent="center" pt="10">
            <Text fontSize={28}>
              {state === 'create'
                ? t('titles.createPinCode')
                : state === 'confirm'
                ? t('titles.confirmPinCode')
                : t('titles.enterPinCode')}
            </Text>
          </HStack>

          <Spacer />

          <HStack space="2" justifyContent="center">
            {Array.from({ length: PIN_LENGTH }).map((_, index) => (
              <Box
                key={`pin-box-${index}`}
                w="6"
                h="6"
                rounded="md"
                bg={pin[index] ? 'primary.600' : 'primaryAlpha.50'}
                borderWidth={1}
                borderColor={pin[index] ? 'primary.600' : 'primaryAlpha.50'}
                _dark={{
                  bg: pin[index] ? 'primary.500' : 'whiteAlpha.50',
                  borderColor: pin[index] ? 'primary.500' : 'whiteAlpha.50',
                }}
              />
            ))}
          </HStack>

          <Spacer />

          <VStack w="full" space={2}>
            <HStack space={2}>
              {renderNumberButton('1')}
              {renderNumberButton('2')}
              {renderNumberButton('3')}
            </HStack>
            <HStack space={2}>
              {renderNumberButton('4')}
              {renderNumberButton('5')}
              {renderNumberButton('6')}
            </HStack>
            <HStack space={2}>
              {renderNumberButton('7')}
              {renderNumberButton('8')}
              {renderNumberButton('9')}
            </HStack>
            <HStack space={2}>
              {biometricType !== null && state === 'enter' ? (
                <Button
                  flex={1}
                  variant="subtle"
                  _dark={{ bgColor: 'darkGrayAlpha.500' }}
                  onPress={unlockByBiometrics}>
                  {biometricType === 'touchId' ? (
                    <Icon.TouchId />
                  ) : (
                    <Icon.FaceId />
                  )}
                </Button>
              ) : (
                <Button flex={1} opacity={0} disabled />
              )}
              {renderNumberButton('0')}
              <Button
                flex={1}
                variant="subtle"
                _dark={{ bgColor: 'darkGrayAlpha.500' }}
                onPress={() => pin.length > 0 && setPin(pin.slice(0, -1))}>
                <Icon.ArrowLeft />
              </Button>
            </HStack>
          </VStack>
        </VStack>
      </SafeAreaView>
    </VStack>
  ) : null;
};

export { PinCodeLock };
