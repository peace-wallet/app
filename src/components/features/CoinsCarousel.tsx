import {
  Box,
  Button,
  HStack,
  Icon,
  Pressable,
  Spacer,
  Text,
  VStack,
} from '../base';
import { useNavigation } from '@react-navigation/core';
import { useTheme } from 'native-base';
import numeral from 'numeral';
import React, { FC, useRef } from 'react';
import { useTranslation } from 'react-i18next';
import { Dimensions, StyleSheet } from 'react-native';
import LinearGradient from 'react-native-linear-gradient';
import Carousel from 'react-native-reanimated-carousel';

import { Balance, Coin } from '../../models';
import { balanceByCoin } from '../../utils/balance';
import { getCoins } from '../../utils/coins';

export type CoinsCarouselProps = {
  balances: Balance[];
  initialIndex: number;
  onSnapToItem?(item: Coin): void;
  onLockedBalancePress?(item: Coin): void;
  onScrollBegin?(): void;
  onScrollEnd?(): void;
};

const CoinsCarousel: FC<CoinsCarouselProps> = props => {
  const {
    balances,
    initialIndex = 1,
    onSnapToItem = () => null,
    onScrollBegin = () => null,
    onScrollEnd = () => null,
    onLockedBalancePress = () => null,
  } = props;

  const { t } = useTranslation();

  const coins = getCoins();

  const navigation = useNavigation();
  const { colors } = useTheme();

  const carouselRef = useRef(null);

  let timeout: NodeJS.Timeout;

  const SLIDER_WIDTH = Dimensions.get('window').width;

  const handleSnapToItem = async (index: number) => {
    clearTimeout(timeout);
    timeout = setTimeout(() => {
      onSnapToItem(coins[index]);
    }, 150);
  };

  const onReceive = async (coin: Coin) => {
    navigation.navigate('TabsStack', {
      screen: 'BalanceStack',
      params: {
        screen: 'ReceiveStack',
        params: {
          screen: 'Receive',
          params: {
            shardId: coin.shardId,
          },
        },
      },
    });
  };

  const onSend = async (coin: Coin) => {
    navigation.navigate('TabsStack', {
      screen: 'BalanceStack',
      params: {
        screen: 'SendStack',
        params: {
          screen: 'Send',
          params: {
            shardId: coin.shardId,
          },
        },
      },
    });
  };

  const renderCarouselItem = (index: number) => {
    const coin = coins[index];

    const availableBalance = balanceByCoin(coin, balances).available;
    const lockedBalance = balanceByCoin(coin, balances).locked;

    const formattedAvailableBalance =
      availableBalance > 10000
        ? numeral(availableBalance).format('0.00a')
        : availableBalance;
    const formattedLockedBalance =
      lockedBalance > 10000
        ? numeral(lockedBalance).format('0.00a')
        : lockedBalance;

    return (
      <Box
        height={200}
        rounded="xl"
        ml={2}
        mr={index === coins.length - 1 ? 2 : 1.5}
        overflow="hidden"
        bg="t">
        <VStack flex={1}>
          <LinearGradient
            colors={[
              colors.coins[coin.name][100],
              colors.coins[coin.name][300],
            ]}
            locations={[0, 0.8]}
            useAngle={true}
            angle={index === 1 ? 52 : 130}
            style={styles.gradient}>
            <Box flex={1} p="2">
              <VStack flex={1}>
                <HStack px="2" pt="2">
                  <Text color="primary.500" opacity={0.6}>
                    {coin.title}
                  </Text>
                </HStack>

                <Spacer />

                <VStack px="2">
                  <HStack>
                    <Text color="primary.500" fontSize={28}>
                      {formattedAvailableBalance}
                    </Text>
                  </HStack>

                  {lockedBalance > 0 && (
                    <Pressable onPress={() => onLockedBalancePress(coin)}>
                      <HStack
                        alignItems="center"
                        mt={-1}
                        alignContent="center"
                        space={1}>
                        <Text
                          color="primaryAlpha.500"
                          secondary
                          lineHeight={16}>
                          {t('texts.includingLocked', {
                            balance: formattedLockedBalance,
                          })}
                        </Text>
                        <Icon.ChevronRight
                          color="primaryAlpha.500"
                          size={4}
                          opacity={0.8}
                        />
                      </HStack>
                    </Pressable>
                  )}
                </VStack>

                <Spacer />

                <HStack space="xs">
                  <Button
                    flex={1}
                    variant="outline"
                    endIcon={<Icon.ArrowBottomLeft color="primary.500" />}
                    _light={{
                      backgroundColor: 'whiteAlpha.200',
                      borderColor: 'whiteAlpha.100',
                    }}
                    _dark={{
                      backgroundColor: 'whiteAlpha.200',
                      borderColor: 'whiteAlpha.100',
                    }}
                    _text={{ color: 'primary.500' }}
                    onPress={() => onReceive(coin)}>
                    {t('buttons.receive')}
                  </Button>
                  <Button
                    flex={1}
                    variant="outline"
                    endIcon={<Icon.ArrowTopRight color="primary.500" />}
                    _light={{
                      backgroundColor: 'whiteAlpha.200',
                      borderColor: 'whiteAlpha.100',
                    }}
                    _dark={{
                      backgroundColor: 'whiteAlpha.200',
                      borderColor: 'whiteAlpha.100',
                    }}
                    _text={{ color: 'primary.500' }}
                    onPress={() => onSend(coin)}>
                    {t('buttons.send')}
                  </Button>
                </HStack>
              </VStack>
            </Box>
          </LinearGradient>
        </VStack>
      </Box>
    );
  };

  return (
    <Carousel
      defaultIndex={initialIndex}
      ref={carouselRef}
      width={SLIDER_WIDTH}
      height={200}
      data={[...new Array(coins.length).keys()]}
      mode="parallax"
      modeConfig={{
        parallaxScrollingScale: 0.9,
        parallaxScrollingOffset: 60,
      }}
      renderItem={({ index }) => renderCarouselItem(index)}
      onSnapToItem={handleSnapToItem}
      onScrollBegin={onScrollBegin}
      onScrollEnd={onScrollEnd}
    />
  );
};

const styles = StyleSheet.create({
  gradient: {
    height: '100%',
  },
});

export { CoinsCarousel };
