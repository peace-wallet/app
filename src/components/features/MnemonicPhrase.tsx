import { Box, HStack, Text, VStack } from '../base';
import React, { FC } from 'react';
import { StyleSheet } from 'react-native';

export type MnemonicPhraseProps = {
  mnemonic: string;
};

const MnemonicPhrase: FC<MnemonicPhraseProps> = props => {
  const { mnemonic } = props;

  const renderPhrases = () => {
    const rows = [];
    let cols = [];

    for (let i = 0; i <= mnemonic.split(' ').length; i++) {
      const isEndOfLine = (i + 1) % 3 === 0;

      const col = (
        <Box
          flex={1}
          key={`col-${i}`}
          rounded="lg"
          py="3"
          style={styles.tile}
          _light={{
            backgroundColor: 'white',
            borderColor: 'primaryAlpha.50',
            _android: {
              borderColor: 'primaryAlpha.100',
            },
          }}
          _dark={{
            backgroundColor: 'darkGray.500',
            borderColor: 'whiteAlpha.50',
            _android: {
              borderColor: 'whiteAlpha.100',
            },
          }}>
          <HStack alignItems="center" justifyContent="center" space="xs">
            <Text
              _light={{
                color: 'primaryAlpha.500',
              }}
              _dark={{
                color: 'whiteAlpha.500',
              }}>{`${i + 1}.`}</Text>
            <Text>{mnemonic.split(' ')[i]}</Text>
          </HStack>
        </Box>
      );

      cols.push(col);

      if (isEndOfLine) {
        rows.push(
          <HStack space="xs" key={`row-${i}`}>
            {cols}
          </HStack>,
        );
        cols = [];
      }
    }

    return rows;
  };

  if (!mnemonic) return null;

  return <VStack space="xs">{renderPhrases()}</VStack>;
};

const styles = StyleSheet.create({
  tile: {
    borderWidth: 1,
  },
});

export { MnemonicPhrase };
