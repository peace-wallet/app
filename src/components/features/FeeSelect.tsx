import { Box, Flex, HStack, Pressable, Text, VStack } from '../base';
import { useColorModeValue } from 'native-base';
import React, { FC } from 'react';
import { useTranslation } from 'react-i18next';

import { Coin, Fee } from '../../models';
import { CoinsFormatter } from '../../utils/formatters';

export type FeeSelectProps = {
  fees: Fee[];
  value: Fee;
  coin: Coin;
  onChange(fee: Fee): void;
};

const FeeSelect: FC<FeeSelectProps> = props => {
  const { fees, value, coin, onChange } = props;

  const { t } = useTranslation();

  const activeFeeBackgroundColorToken = useColorModeValue(
    'primaryAlpha.50',
    'whiteAlpha.50',
  );
  const inactiveFeeBackgroundColorToken = useColorModeValue(
    'offWhite',
    'darkGray.500',
  );

  return (
    <Box rounded="xl" overflow="hidden">
      <HStack alignItems="center" justifyContent="center">
        {fees.map(fee => {
          const feeValue = fee.value
            ? CoinsFormatter.toBiggestUnits(coin.name, fee.value)
            : 'N/A';

          return (
            <Flex key={`fee-${fee.name}`} flex={1}>
              <Pressable onPress={() => onChange(fee)}>
                <Box
                  py="4"
                  bgColor={
                    fee.name === value.name
                      ? activeFeeBackgroundColorToken
                      : inactiveFeeBackgroundColorToken
                  }>
                  <VStack space="sm" alignItems="center">
                    <Text>
                      {t(
                        `labels.fee${
                          (fee.name as string).charAt(0).toUpperCase() +
                          fee.name.slice(1)
                        }` as keyof typeof t,
                      )}
                    </Text>
                    <HStack alignItems="center">
                      <Text secondary>{`${feeValue} ${coin.title}`}</Text>
                    </HStack>
                  </VStack>
                </Box>
              </Pressable>
            </Flex>
          );
        })}
      </HStack>
    </Box>
  );
};

export { FeeSelect };
