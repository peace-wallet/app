import { Screen as BaseScreen, ScreenProps } from '../base';
import React from 'react';
import { useTranslation } from 'react-i18next';

import { useAppStore } from '../../stores';

export const Screen = (props: ScreenProps) => {
  const { t } = useTranslation();

  const network = useAppStore(state => state.network);

  return (
    <BaseScreen
      {...props}
      warningText={network === 'testnet' && t('texts.testnet')}
    />
  );
};
