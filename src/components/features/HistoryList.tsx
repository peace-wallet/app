import {
  Box,
  Card,
  HStack,
  Icon,
  Pressable,
  RefreshControl,
  Spinner,
  Text,
  VStack,
} from '../base';
import moment from 'moment';
import { theme } from 'native-base';
import numeral from 'numeral';
import React, { useMemo, useState } from 'react';
import { useTranslation } from 'react-i18next';
import { Dimensions, Platform, StyleSheet } from 'react-native';
import BigList from 'react-native-big-list';

import { useBtcBalance, useHistory, useJaxBalances } from '../../hooks';
import { Coin, HistoryItem } from '../../models';
import { queryClient } from '../../services/react-query';
import { useAppStore } from '../../stores';
import { balancesByCoin } from '../../utils/balance';
import { getCoins } from '../../utils/coins';
import { CoinsFormatter } from '../../utils/formatters';
import { CoinsCarousel } from './CoinsCarousel';

type HistoryListProps = {
  onSelectCoin: (coin: Coin) => void;
  onSelectHistoryItem: (historyItem: HistoryItem) => void;
};

const HistoryListBase = ({
  onSelectCoin = () => null,
  onSelectHistoryItem = () => null,
}: HistoryListProps) => {
  const { t } = useTranslation();

  const network = useAppStore(state => state.network);
  const address = useAppStore(state => state.address);

  const [coin, setCoin] = useState(getCoins()[1]);

  // const { data: btcBalance, refetch: refetchBtcBalance } = useBtcBalance(
  //   address,
  //   network,
  // );
  const { data: jaxBalances, refetch: refetchJaxBalances } = useJaxBalances(
    address,
    network,
  );

  const balances = useMemo(
    () => [
      // btcBalance,
      ...jaxBalances,
    ],
    [
      // btcBalance,
      jaxBalances,
    ],
  );

  const shardIds = useMemo(
    () => balancesByCoin(coin, balances).map(balance => balance.shardId),
    [balances, coin],
  );

  const {
    data: historyData,
    refetch: refetchHistory,
    fetchNextPage: fetchNextHistoryPage,
    isLoading: historyIsLoading,
    isFetching: historyIsFetching,
    isFetchingNextPage: historyIsFetchingNextPage,
    dataUpdatedAt: historyUpdatedAt,
  } = useHistory(address, shardIds, network);

  const history = useMemo(
    () => historyData?.pages.flat(),
    [historyData?.pages],
  );

  const isLoadedAll = useMemo(
    () =>
      !(historyData?.pageParams[historyData?.pageParams.length - 1] as [])
        ?.map(p => p['loadedAll'] as boolean)
        .includes(false) && historyData?.pages.length > 0,
    [historyData?.pageParams, historyData?.pages.length],
  );

  const [isRefetching, setIsRefetching] = useState(false);

  const footerIsVisible = useMemo(
    () => historyIsFetchingNextPage || isLoadedAll,
    [historyIsFetchingNextPage, isLoadedAll],
  );

  const onRefresh = async () => {
    setIsRefetching(true);
    await Promise.all([refetchJaxBalances(), refetchHistory()]);
    setIsRefetching(false);

    // refetchBtcBalance();
  };

  const onChangeCoin = (coin: Coin) => {
    setCoin(coin);
    onSelectCoin(coin);
  };

  const onEndReached = () => {
    fetchNextHistoryPage({ pageParam: history?.length });
  };

  const formatHistorySectionDate = (date: string) => {
    const momentDate = moment(date);
    const formattedDate = momentDate.format(
      momentDate.year() === moment().year() ? 'D MMM' : 'D MMM YYYY',
    );

    return formattedDate;
  };

  const renderHeader = () => (
    <VStack pt={2} space="lg">
      <CoinsCarousel
        initialIndex={1}
        balances={balances}
        // onLockedBalancePress={navigateToLockedBalanceScreen}
        onSnapToItem={onChangeCoin}
        onScrollBegin={() => queryClient.cancelQueries()}
      />

      <HStack px="6" pb="4" justifyContent="space-between">
        <Text textTransform="uppercase">{t('labels.transactions')}</Text>
        <Text secondary fontWeight={400}>
          {t('labels.updatedAt', {
            time: moment(historyUpdatedAt).format('LT'),
          })}
        </Text>
      </HStack>
    </VStack>
  );

  const renderFooter = () => (
    <HStack
      mx="2"
      mt="3"
      alignContent="center"
      justifyContent="center"
      height="56px">
      {footerIsVisible && historyIsFetchingNextPage ? (
        <Spinner />
      ) : footerIsVisible && isLoadedAll && !historyIsFetching ? (
        <Card flex={1} bordered>
          <Text align="center" secondary>
            {t('texts.loadedAllTransactions')}
          </Text>
        </Card>
      ) : null}
    </HStack>
  );

  const renderHistoryItemIcon = (direction: 'in' | 'out' | 'loop' | string) => {
    let content = null;

    switch (direction) {
      case 'loop':
        content = (
          <>
            <Icon.ArrowTopRight color={`coins.${coin.name}.500`} />
            <Icon.Refresh
              size={3}
              color={`coins.${coin.name}.500`}
              style={styles.listItemLoopIcon}
            />
          </>
        );
        break;
      case 'out':
        content = <Icon.ArrowTopRight color={`coins.${coin.name}.500`} />;
        break;
      case 'in':
        content = <Icon.ArrowBottomLeft color={`coins.${coin.name}.500`} />;
        break;
    }

    return (
      <Box
        p="3"
        rounded="lg"
        borderWidth={1}
        alignItems="center"
        justifyContent="center"
        _light={{ borderColor: 'primaryAlpha.50' }}
        _dark={{ borderColor: 'whiteAlpha.100' }}>
        {content}
      </Box>
    );
  };

  const renderHistoryItem = ({
    item: historyItem,
    index,
  }: {
    item: HistoryItem;
    index: number;
  }) => {
    const sourceAmount = CoinsFormatter.toBiggestUnits(
      coin.name,
      historyItem.crossShardTx ? historyItem.sentAmount : historyItem.amount,
    );
    const formattedSourceAmount =
      sourceAmount > 10000
        ? numeral(sourceAmount).format('0.00a')
        : sourceAmount;

    if (historyItem.amount === 0) return;

    return (
      <Pressable
        key={`list-section-${historyItem.hash}`}
        mx={4}
        p={0}
        py={2}
        onPress={() => onSelectHistoryItem(historyItem)}>
        <HStack alignItems="stretch" space={2}>
          {renderHistoryItemIcon(historyItem.direction)}
          <VStack flex={1} justifyContent="space-around">
            <HStack alignItems="center">
              <HStack flex={1} space={1}>
                {sourceAmount > 10000 ? (
                  <Text>{formattedSourceAmount}</Text>
                ) : (
                  <Text>{sourceAmount}</Text>
                )}
                <Text color={`coins.${coin.name}.500`} fontWeight={400}>
                  {coin.title}
                </Text>
              </HStack>
              <Text
                secondary
                fontWeight={400}
                _light={{
                  color:
                    !historyItem.confirmations ||
                    historyItem.confirmations <= 10
                      ? 'orange.400'
                      : 'primaryAlpha.500',
                }}
                _dark={{
                  color:
                    !historyItem.confirmations ||
                    historyItem.confirmations <= 10
                      ? 'orange.400'
                      : 'whiteAlpha.500',
                }}>
                {t(
                  `labels.status${
                    !historyItem.confirmations
                      ? historyItem.crossShardTx
                        ? //  && historyItem.crossShardTemp
                          'InProgress'
                        : 'Mempool'
                      : historyItem.confirmations > 10
                      ? 'Confirmed'
                      : 'Unconfirmed'
                  }`,
                )}
              </Text>
            </HStack>
            <HStack justifyContent="space-between">
              <Text secondary fontWeight={400}>
                {`${
                  historyItem.direction === 'in'
                    ? t('labels.incoming')
                    : t('labels.outgoing')
                } ${
                  coin.name === 'jax'
                    ? `• ${t('labels.shard')} ${historyItem.shardId}`
                    : ''
                } ${
                  historyItem.crossShardTx
                    ? `→ ${t('labels.shard')} ${historyItem.shardId}`
                    : ''
                }`}
              </Text>
              <Text secondary fontWeight={400}>
                {moment(historyItem.timestamp).format('LT')}
              </Text>
            </HStack>
          </VStack>
        </HStack>
      </Pressable>
    );
  };

  const renderHistoryEmpty = () => (
    <HStack alignContent="center" justifyContent="center" height="56px">
      {historyIsLoading || historyIsFetching ? (
        <Spinner />
      ) : (
        <Card mx="2" bordered flex={1}>
          <Text align="center" secondary>
            {t('texts.emptyOperations')}
          </Text>
        </Card>
      )}
    </HStack>
  );

  // console.log('history.loadedAll', history.loadedAll);

  return (
    <BigList
      sections={history}
      renderHeader={renderHeader}
      renderFooter={renderFooter}
      renderEmpty={renderHistoryEmpty}
      renderItem={renderHistoryItem}
      renderSectionHeader={section => (
        <Text align="center" bg="offWhite" _dark={{ bg: 'dark.500' }} secondary>
          {formatHistorySectionDate(history[section][0].timestamp)}
        </Text>
      )}
      refreshControl={
        <RefreshControl
          refreshing={isRefetching}
          onRefresh={onRefresh}
          title={t('texts.dataRefreshing')}
        />
      }
      itemHeight={66}
      headerHeight={272}
      footerHeight={footerIsVisible ? 68 : 0}
      sectionHeaderHeight={24}
      stickySectionHeadersEnabled
      showsVerticalScrollIndicator={false}
      contentContainerStyle={styles.contentContainer}
      onEndReached={onEndReached}
    />
  );
};

const styles = StyleSheet.create({
  contentContainer: {
    paddingBottom: Platform.OS === 'android' ? 112 : 112,
  },
  listItemLoopIcon: {
    bottom: 4,
    position: 'absolute',
    right: 4,
  },
  listSection: {
    paddingHorizontal: theme.space[2],
  },
});

const HistoryList = React.memo(HistoryListBase);

export { HistoryList };
